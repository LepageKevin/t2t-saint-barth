<?php

namespace App\Test\Services\Formaters;

use App\Services\Formaters\Player;
use PHPUnit\Framework\TestCase;

class PlayerTest extends TestCase {
    private $player;

    public function testFormatSucceed()
    {
        $output = "<liste>
                        <joueur>
                            <licence>xxxL</licence>
                            <nom>xxxN</nom>
                            <prenom>xxxP</prenom>
                            <club>xxxC</club>
                            <nclub>xxxNN</nclub>
                            <clast>xxxCC</clast>
                        </joueur>
                        <joueur>
                            <licence>xxx</licence>
                            <nom>xxx</nom>
                            <prenom>xxx</prenom>
                            <club>xxx</club>
                            <nclub>xxx</nclub>
                            <clast>xxx</clast>
                        </joueur>
                        <joueur>
                            <licence>xxx</licence>
                            <nom>xxx</nom>
                            <prenom>xxx</prenom>
                            <club>xxx</club>
                            <nclub>xxx</nclub>
                            <clast>xxx</clast>
                        </joueur>
                        <joueur>
                            <licence>xxx</licence>
                            <nom>xxx</nom>
                            <prenom>xxx</prenom>
                            <club>xxx</club>
                            <nclub>xxx</nclub>
                            <clast>xxx</clast>
                        </joueur>
                        <joueur>
                            <licence>xxx</licence>
                            <nom>xxx</nom>
                            <prenom>xxx</prenom>
                            <club>xxx</club>
                            <nclub>xxx</nclub>
                            <clast>xxx</clast>
                        </joueur>" .
            "
                    </liste>";

        $arrayPlayers = $this->player::format($output);

        $this->assertInternalType('array', $arrayPlayers);
        $this->assertTrue(is_array($arrayPlayers));
        $this->assertEquals(5, count($arrayPlayers));
        $this->assertArrayHasKey(0, $arrayPlayers);

        $player = $arrayPlayers[0];
        $this->assertObjectHasAttribute('licence', $player);
        $this->assertObjectHasAttribute('nom', $player);
        $this->assertObjectHasAttribute('prenom', $player);
        $this->assertObjectHasAttribute('club', $player);
        $this->assertObjectHasAttribute('nclub', $player);
        $this->assertObjectHasAttribute('clast', $player);

        $this->assertEquals("xxxL", $player->licence);
        $this->assertEquals("xxxN", $player->nom);
        $this->assertEquals("xxxP", $player->prenom);
        $this->assertEquals("xxxC", $player->club);
        $this->assertEquals("xxxNN", $player->nclub);
        $this->assertEquals("xxxCC", $player->clast);
    }

    public function testFormatUniqueSucceed()
    {
        $output = "<liste>
                        <joueur>
                            <licence>xxxL</licence>
                            <nom>xxxN</nom>
                            <prenom>xxxP</prenom>
                            <club>xxxC</club>
                            <nclub>xxxNN</nclub>
                            <clast>xxxCC</clast>
                        </joueur>
                    </liste>";

        $arrayPlayers = $this->player::format($output);

        $this->assertInternalType('array', $arrayPlayers);
        $this->assertTrue(is_array($arrayPlayers));
        $this->assertEquals(1, count($arrayPlayers));
        $this->assertArrayHasKey(0, $arrayPlayers);

        $player = $arrayPlayers[0];
        $this->assertObjectHasAttribute('licence', $player);
        $this->assertObjectHasAttribute('nom', $player);
        $this->assertObjectHasAttribute('prenom', $player);
        $this->assertObjectHasAttribute('club', $player);
        $this->assertObjectHasAttribute('nclub', $player);
        $this->assertObjectHasAttribute('clast', $player);

        $this->assertEquals("xxxL", $player->licence);
        $this->assertEquals("xxxN", $player->nom);
        $this->assertEquals("xxxP", $player->prenom);
        $this->assertEquals("xxxC", $player->club);
        $this->assertEquals("xxxNN", $player->nclub);
        $this->assertEquals("xxxCC", $player->clast);
    }

    /**
     * @expectedException \Exception
     */
    public function testFormatFailure()
    {
        $output = "<joueur>
                        <licence>xxxL</licence>
                        <nom>xxxN</nom>
                        <prenom>xxxP</prenom>
                        <club>xxxC</club>
                        <nclub>xxxNN</nclub>
                        <clast>xxxCC</clast>
                    </joueur>
                    <joueur>
                        <licence>xxx</licence>
                        <nom>xxx</nom>
                        <prenom>xxx</prenom>
                        <club>xxx</club>
                        <nclub>xxx</nclub>
                        <clast>xxx</clast>
                    </joueur>
                    <joueur>
                        <licence>xxx</licence>
                        <nom>xxx</nom>
                        <prenom>xxx</prenom>
                        <club>xxx</club>
                        <nclub>xxx</nclub>
                        <clast>xxx</clast>
                    </joueur>
                    <joueur>
                        <licence>xxx</licence>
                        <nom>xxx</nom>
                        <prenom>xxx</prenom>
                        <club>xxx</club>
                        <nclub>xxx</nclub>
                        <clast>xxx</clast>
                    </joueur>
                    <joueur>
                        <licence>xxx</licence>
                        <nom>xxx</nom>
                        <prenom>xxx</prenom>
                        <club>xxx</club>
                        <nclub>xxx</nclub>
                        <clast>xxx</clast>
                    </joueur>";

        $arrayPlayers = $this->player::format($output);
    }

    public function testFormatUniqueFailure()
    {
        $output = "<joueur>
                        <licence>xxxL</licence>
                        <nom>xxxN</nom>
                        <prenom>xxxP</prenom>
                        <club>xxxC</club>
                        <nclub>xxxNN</nclub>
                        <clast>xxxCC</clast>
                    </joueur>";

        $arrayPlayers = $this->player::format($output);

        $this->assertInternalType('array', $arrayPlayers);
        $this->assertTrue(is_array($arrayPlayers));
        $this->assertEquals(0, count($arrayPlayers));
        $this->assertArrayNotHasKey(0, $arrayPlayers);
    }

    public function setUp()
    {
        $this->player = new Player();
        parent::setUp(); // TODO: Change the autogenerated stub
    }
}