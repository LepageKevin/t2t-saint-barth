<?php

namespace App\Test\Services\Formaters;

use App\Services\Formaters\Member;
use PHPUnit\Framework\TestCase;

class MemberTest extends TestCase {
    private $member;

    public function testFormatSucceed()
    {
        $output = "<liste>
                        <licence>
                            <idlicence>xxxI</idlicence>
                            <nom>xxxN</nom>
                            <prenom>xxxP</prenom>
                            <licence>xxxL</licence>
                            <numclub>xxxNN</numclub>
                            <nomclub>xxxNNN</nomclub>
                            <sexe>xxxS</sexe>
                            <type>xxxT</type>
                            <certif>xxxC</certif>
                            <validation>xxxV</validation>
                            <echelon>xxxE</echelon>
                            <place>xxxPP</place>
                            <point>xxxPPP</point>
                            <cat>xxxCC</cat>
                        </licence>
                        <licence>
                            <idlicence>xxx</idlicence>
                            <nom>xxx</nom>
                            <prenom>xxx</prenom>
                            <licence>xxx</licence>
                            <numclub>xxx</numclub>
                            <nomclub>xxx</nomclub>
                            <sexe>xxx</sexe>
                            <type>xxx</type>
                            <certif>xxx</certif>
                            <validation>xxx</validation>
                            <echelon>xxx</echelon>
                            <place>xxx</place>
                            <point>xxx</point>
                            <cat>xxx</cat>
                        </licence>
                        <licence>
                            <idlicence>xxx</idlicence>
                            <nom>xxx</nom>
                            <prenom>xxx</prenom>
                            <licence>xxx</licence>
                            <numclub>xxx</numclub>
                            <nomclub>xxx</nomclub>
                            <sexe>xxx</sexe>
                            <type>xxx</type>
                            <certif>xxx</certif>
                            <validation>xxx</validation>
                            <echelon>xxx</echelon>
                            <place>xxx</place>
                            <point>xxx</point>
                            <cat>xxx</cat>
                        </licence>
                        <licence>
                            <idlicence>xxx</idlicence>
                            <nom>xxx</nom>
                            <prenom>xxx</prenom>
                            <licence>xxx</licence>
                            <numclub>xxx</numclub>
                            <nomclub>xxx</nomclub>
                            <sexe>xxx</sexe>
                            <type>xxx</type>
                            <certif>xxx</certif>
                            <validation>xxx</validation>
                            <echelon>xxx</echelon>
                            <place>xxx</place>
                            <point>xxx</point>
                            <cat>xxx</cat>
                        </licence>
                        <licence>
                            <idlicence>xxx</idlicence>
                            <nom>xxx</nom>
                            <prenom>xxx</prenom>
                            <licence>xxx</licence>
                            <numclub>xxx</numclub>
                            <nomclub>xxx</nomclub>
                            <sexe>xxx</sexe>
                            <type>xxx</type>
                            <certif>xxx</certif>
                            <validation>xxx</validation>
                            <echelon>xxx</echelon>
                            <place>xxx</place>
                            <point>xxx</point>
                            <cat>xxx</cat>
                        </licence>
                        <licence>
                            <idlicence>xxx</idlicence>
                            <nom>xxx</nom>
                            <prenom>xxx</prenom>
                            <licence>xxx</licence>
                            <numclub>xxx</numclub>
                            <nomclub>xxx</nomclub>
                            <sexe>xxx</sexe>
                            <type>xxx</type>
                            <certif>xxx</certif>
                            <validation>xxx</validation>
                            <echelon>xxx</echelon>
                            <place>xxx</place>
                            <point>xxx</point>
                            <cat>xxx</cat>
                        </licence>
                        <licence>
                            <idlicence>xxx</idlicence>
                            <nom>xxx</nom>
                            <prenom>xxx</prenom>
                            <licence>xxx</licence>
                            <numclub>xxx</numclub>
                            <nomclub>xxx</nomclub>
                            <sexe>xxx</sexe>
                            <type>xxx</type>
                            <certif>xxx</certif>
                            <validation>xxx</validation>
                            <echelon>xxx</echelon>
                            <place>xxx</place>
                            <point>xxx</point>
                            <cat>xxx</cat>
                        </licence>
                        <licence>
                            <idlicence>xxx</idlicence>
                            <nom>xxx</nom>
                            <prenom>xxx</prenom>
                            <licence>xxx</licence>
                            <numclub>xxx</numclub>
                            <nomclub>xxx</nomclub>
                            <sexe>xxx</sexe>
                            <type>xxx</type>
                            <certif>xxx</certif>
                            <validation>xxx</validation>
                            <echelon>xxx</echelon>
                            <place>xxx</place>
                            <point>xxx</point>
                            <cat>xxx</cat>
                        </licence>
                    </liste>";

        $arrayMembers = $this->member::format($output);

        $this->assertInternalType('array', $arrayMembers);
        $this->assertTrue(is_array($arrayMembers));
        $this->assertEquals(8, count($arrayMembers));
        $this->assertArrayHasKey(0, $arrayMembers);

        $member = $arrayMembers[0];
        $this->assertObjectHasAttribute('idlicence', $member);
        $this->assertObjectHasAttribute('nom', $member);
        $this->assertObjectHasAttribute('prenom', $member);
        $this->assertObjectHasAttribute('licence', $member);
        $this->assertObjectHasAttribute('numclub', $member);
        $this->assertObjectHasAttribute('nomclub', $member);
        $this->assertObjectHasAttribute('sexe', $member);
        $this->assertObjectHasAttribute('type', $member);
        $this->assertObjectHasAttribute('certif', $member);
        $this->assertObjectHasAttribute('validation', $member);
        $this->assertObjectHasAttribute('echelon', $member);
        $this->assertObjectHasAttribute('place', $member);
        $this->assertObjectHasAttribute('point', $member);
        $this->assertObjectHasAttribute('cat', $member);

        $this->assertEquals("xxxI", $member->idlicence);
        $this->assertEquals("xxxN", $member->nom);
        $this->assertEquals("xxxP", $member->prenom);
        $this->assertEquals("xxxL", $member->licence);
        $this->assertEquals("xxxNN", $member->numclub);
        $this->assertEquals("xxxNNN", $member->nomclub);
        $this->assertEquals("xxxS", $member->sexe);
        $this->assertEquals("xxxT", $member->type);
        $this->assertEquals("xxxC", $member->certif);
        $this->assertEquals("xxxV", $member->validation);
        $this->assertEquals("xxxE", $member->echelon);
        $this->assertEquals("xxxPP", $member->place);
        $this->assertEquals("xxxPPP", $member->point);
        $this->assertEquals("xxxCC", $member->cat);
    }

    public function testFormatUniqueSucceed()
    {
        $output = "<liste>
                        <licence>
                            <idlicence>xxxI</idlicence>
                            <nom>xxxN</nom>
                            <prenom>xxxP</prenom>
                            <licence>xxxL</licence>
                            <numclub>xxxNN</numclub>
                            <nomclub>xxxNNN</nomclub>
                            <sexe>xxxS</sexe>
                            <type>xxxT</type>
                            <certif>xxxC</certif>
                            <validation>xxxV</validation>
                            <echelon>xxxE</echelon>
                            <place>xxxPP</place>
                            <point>xxxPPP</point>
                            <cat>xxxCC</cat>
                        </licence>
                    </liste>";

        $arrayMembers = $this->member::format($output);

        $this->assertInternalType('array', $arrayMembers);
        $this->assertTrue(is_array($arrayMembers));
        $this->assertEquals(1, count($arrayMembers));
        $this->assertArrayHasKey(0, $arrayMembers);

        $member = $arrayMembers[0];
        $this->assertObjectHasAttribute('idlicence', $member);
        $this->assertObjectHasAttribute('nom', $member);
        $this->assertObjectHasAttribute('prenom', $member);
        $this->assertObjectHasAttribute('licence', $member);
        $this->assertObjectHasAttribute('numclub', $member);
        $this->assertObjectHasAttribute('nomclub', $member);
        $this->assertObjectHasAttribute('sexe', $member);
        $this->assertObjectHasAttribute('type', $member);
        $this->assertObjectHasAttribute('certif', $member);
        $this->assertObjectHasAttribute('validation', $member);
        $this->assertObjectHasAttribute('echelon', $member);
        $this->assertObjectHasAttribute('place', $member);
        $this->assertObjectHasAttribute('point', $member);
        $this->assertObjectHasAttribute('cat', $member);

        $this->assertEquals("xxxI", $member->idlicence);
        $this->assertEquals("xxxN", $member->nom);
        $this->assertEquals("xxxP", $member->prenom);
        $this->assertEquals("xxxL", $member->licence);
        $this->assertEquals("xxxNN", $member->numclub);
        $this->assertEquals("xxxNNN", $member->nomclub);
        $this->assertEquals("xxxS", $member->sexe);
        $this->assertEquals("xxxT", $member->type);
        $this->assertEquals("xxxC", $member->certif);
        $this->assertEquals("xxxV", $member->validation);
        $this->assertEquals("xxxE", $member->echelon);
        $this->assertEquals("xxxPP", $member->place);
        $this->assertEquals("xxxPPP", $member->point);
        $this->assertEquals("xxxCC", $member->cat);
    }

    /**
     * @expectedException \Exception
     */
    public function testFormatFailure()
    {
        $output = "<licence>
                        <idlicence>xxxI</idlicence>
                        <nom>xxxN</nom>
                        <prenom>xxxP</prenom>
                        <licence>xxxL</licence>
                        <numclub>xxxNN</numclub>
                        <nomclub>xxxNNN</nomclub>
                        <sexe>xxxS</sexe>
                        <type>xxxT</type>
                        <certif>xxxC</certif>
                        <validation>xxxV</validation>
                        <echelon>xxxE</echelon>
                        <place>xxxPP</place>
                        <point>xxxPPP</point>
                        <cat>xxxCC</cat>
                    </licence>
                    <licence>
                        <idlicence>xxx</idlicence>
                        <nom>xxx</nom>
                        <prenom>xxx</prenom>
                        <licence>xxx</licence>
                        <numclub>xxx</numclub>
                        <nomclub>xxx</nomclub>
                        <sexe>xxx</sexe>
                        <type>xxx</type>
                        <certif>xxx</certif>
                        <validation>xxx</validation>
                        <echelon>xxx</echelon>
                        <place>xxx</place>
                        <point>xxx</point>
                        <cat>xxx</cat>
                    </licence>
                    <licence>
                        <idlicence>xxx</idlicence>
                        <nom>xxx</nom>
                        <prenom>xxx</prenom>
                        <licence>xxx</licence>
                        <numclub>xxx</numclub>
                        <nomclub>xxx</nomclub>
                        <sexe>xxx</sexe>
                        <type>xxx</type>
                        <certif>xxx</certif>
                        <validation>xxx</validation>
                        <echelon>xxx</echelon>
                        <place>xxx</place>
                        <point>xxx</point>
                        <cat>xxx</cat>
                    </licence>
                    <licence>
                        <idlicence>xxx</idlicence>
                        <nom>xxx</nom>
                        <prenom>xxx</prenom>
                        <licence>xxx</licence>
                        <numclub>xxx</numclub>
                        <nomclub>xxx</nomclub>
                        <sexe>xxx</sexe>
                        <type>xxx</type>
                        <certif>xxx</certif>
                        <validation>xxx</validation>
                        <echelon>xxx</echelon>
                        <place>xxx</place>
                        <point>xxx</point>
                        <cat>xxx</cat>
                    </licence>
                    <licence>
                        <idlicence>xxx</idlicence>
                        <nom>xxx</nom>
                        <prenom>xxx</prenom>
                        <licence>xxx</licence>
                        <numclub>xxx</numclub>
                        <nomclub>xxx</nomclub>
                        <sexe>xxx</sexe>
                        <type>xxx</type>
                        <certif>xxx</certif>
                        <validation>xxx</validation>
                        <echelon>xxx</echelon>
                        <place>xxx</place>
                        <point>xxx</point>
                        <cat>xxx</cat>
                    </licence>
                    <licence>
                        <idlicence>xxx</idlicence>
                        <nom>xxx</nom>
                        <prenom>xxx</prenom>
                        <licence>xxx</licence>
                        <numclub>xxx</numclub>
                        <nomclub>xxx</nomclub>
                        <sexe>xxx</sexe>
                        <type>xxx</type>
                        <certif>xxx</certif>
                        <validation>xxx</validation>
                        <echelon>xxx</echelon>
                        <place>xxx</place>
                        <point>xxx</point>
                        <cat>xxx</cat>
                    </licence>
                    <licence>
                        <idlicence>xxx</idlicence>
                        <nom>xxx</nom>
                        <prenom>xxx</prenom>
                        <licence>xxx</licence>
                        <numclub>xxx</numclub>
                        <nomclub>xxx</nomclub>
                        <sexe>xxx</sexe>
                        <type>xxx</type>
                        <certif>xxx</certif>
                        <validation>xxx</validation>
                        <echelon>xxx</echelon>
                        <place>xxx</place>
                        <point>xxx</point>
                        <cat>xxx</cat>
                    </licence>
                    <licence>
                        <idlicence>xxx</idlicence>
                        <nom>xxx</nom>
                        <prenom>xxx</prenom>
                        <licence>xxx</licence>
                        <numclub>xxx</numclub>
                        <nomclub>xxx</nomclub>
                        <sexe>xxx</sexe>
                        <type>xxx</type>
                        <certif>xxx</certif>
                        <validation>xxx</validation>
                        <echelon>xxx</echelon>
                        <place>xxx</place>
                        <point>xxx</point>
                        <cat>xxx</cat>
                    </licence>";

        $arrayMembers = $this->member::format($output);
    }

    public function testFormatUniqueFailure()
    {
        $output = "<licence>
                        <idlicence>xxxI</idlicence>
                        <nom>xxxN</nom>
                        <prenom>xxxP</prenom>
                        <licence>xxxL</licence>
                        <numclub>xxxNN</numclub>
                        <nomclub>xxxNNN</nomclub>
                        <sexe>xxxS</sexe>
                        <type>xxxT</type>
                        <certif>xxxC</certif>
                        <validation>xxxV</validation>
                        <echelon>xxxE</echelon>
                        <place>xxxPP</place>
                        <point>xxxPPP</point>
                        <cat>xxxCC</cat>
                    </licence>";

        $arrayMembers = $this->member::format($output);
        $this->assertInternalType('array', $arrayMembers);
        $this->assertTrue(is_array($arrayMembers));
        $this->assertEquals(0, count($arrayMembers));
        $this->assertArrayNotHasKey(0, $arrayMembers);
    }

    public function setUp()
    {
        $this->member = new Member();
        parent::setUp(); // TODO: Change the autogenerated stub
    }
}