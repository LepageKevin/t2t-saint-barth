<?php

namespace App\Test\Services\Formaters;

use App\Services\Formaters\Game;
use PHPUnit\Framework\TestCase;

class GameTest extends TestCase {
    private $game;

    public function testFormatSucceed()
    {
        $output = "<liste>
                        <partie>
                            <licence>xxxL</licence>
                            <advlic>xxxA</advlic>
                            <vd>xxxV</vd>
                            <numjourn>xxxN</numjourn>
                            <codechamp>xxxC</codechamp>
                            <date>xxxD</date>
                            <advsexe>xxxAA</advsexe>
                            <advnompre>xxxAAA</advnompre>
                            <pointres>xxxP</pointres>
                            <coefchamp>xxxCC</coefchamp>
                            <advclaof>xxxAAAA</advclaof>
                            <idpartie>xxxI</idpartie>
                        </partie>
                        <partie>
                            <licence>xxx</licence>
                            <advlic>xxx</advlic>
                            <vd>xxx</vd>
                            <numjourn>xxx</numjourn>
                            <codechamp>xxx</codechamp>
                            <date>xxx</date>
                            <advsexe>xxx</advsexe>
                            <advnompre>xxx</advnompre>
                            <pointres>xxx</pointres>
                            <coefchamp>xxx</coefchamp>
                            <advclaof>xxx</advclaof>
                            <idpartie>xxx</idpartie>
                        </partie>
                        <partie>
                            <licence>xxx</licence>
                            <advlic>xxx</advlic>
                            <vd>xxx</vd>
                            <numjourn>xxx</numjourn>
                            <codechamp>xxx</codechamp>
                            <date>xxx</date>
                            <advsexe>xxx</advsexe>
                            <advnompre>xxx</advnompre>
                            <pointres>xxx</pointres>
                            <coefchamp>xxx</coefchamp>
                            <advclaof>xxx</advclaof>
                            <idpartie>xxx</idpartie>
                        </partie>
                        <partie>
                            <licence>xxx</licence>
                            <advlic>xxx</advlic>
                            <vd>xxx</vd>
                            <numjourn>xxx</numjourn>
                            <codechamp>xxx</codechamp>
                            <date>xxx</date>
                            <advsexe>xxx</advsexe>
                            <advnompre>xxx</advnompre>
                            <pointres>xxx</pointres>
                            <coefchamp>xxx</coefchamp>
                            <advclaof>xxx</advclaof>
                            <idpartie>xxx</idpartie>
                        </partie>
                        <partie>
                            <licence>xxx</licence>
                            <advlic>xxx</advlic>
                            <vd>xxx</vd>
                            <numjourn>xxx</numjourn>
                            <codechamp>xxx</codechamp>
                            <date>xxx</date>
                            <advsexe>xxx</advsexe>
                            <advnompre>xxx</advnompre>
                            <pointres>xxx</pointres>
                            <coefchamp>xxx</coefchamp>
                            <advclaof>xxx</advclaof>
                            <idpartie>xxx</idpartie>
                        </partie>
                        <partie>
                            <licence>xxx</licence>
                            <advlic>xxx</advlic>
                            <vd>xxx</vd>
                            <numjourn>xxx</numjourn>
                            <codechamp>xxx</codechamp>
                            <date>xxx</date>
                            <advsexe>xxx</advsexe>
                            <advnompre>xxx</advnompre>
                            <pointres>xxx</pointres>
                            <coefchamp>xxx</coefchamp>
                            <advclaof>xxx</advclaof>
                            <idpartie>xxx</idpartie>
                        </partie>
                        <partie>
                            <licence>xxx</licence>
                            <advlic>xxx</advlic>
                            <vd>xxx</vd>
                            <numjourn>xxx</numjourn>
                            <codechamp>xxx</codechamp>
                            <date>xxx</date>
                            <advsexe>xxx</advsexe>
                            <advnompre>xxx</advnompre>
                            <pointres>xxx</pointres>
                            <coefchamp>xxx</coefchamp>
                            <advclaof>xxx</advclaof>
                            <idpartie>xxx</idpartie>
                        </partie>
                        <partie>
                            <licence>xxx</licence>
                            <advlic>xxx</advlic>
                            <vd>xxx</vd>
                            <numjourn>xxx</numjourn>
                            <codechamp>xxx</codechamp>
                            <date>xxx</date>
                            <advsexe>xxx</advsexe>
                            <advnompre>xxx</advnompre>
                            <pointres>xxx</pointres>
                            <coefchamp>xxx</coefchamp>
                            <advclaof>xxx</advclaof>
                            <idpartie>xxx</idpartie>
                        </partie>
                        <partie>
                            <licence>xxx</licence>
                            <advlic>xxx</advlic>
                            <vd>xxx</vd>
                            <numjourn>xxx</numjourn>
                            <codechamp>xxx</codechamp>
                            <date>xxx</date>
                            <advsexe>xxx</advsexe>
                            <advnompre>xxx</advnompre>
                            <pointres>xxx</pointres>
                            <coefchamp>xxx</coefchamp>
                            <advclaof>xxx</advclaof>
                            <idpartie>xxx</idpartie>
                        </partie>
                        <partie>
                            <licence>xxx</licence>
                            <advlic>xxx</advlic>
                            <vd>xxx</vd>
                            <numjourn>xxx</numjourn>
                            <codechamp>xxx</codechamp>
                            <date>xxx</date>
                            <advsexe>xxx</advsexe>
                            <advnompre>xxx</advnompre>
                            <pointres>xxx</pointres>
                            <coefchamp>xxx</coefchamp>
                            <advclaof>xxx</advclaof>
                            <idpartie>xxx</idpartie>
                        </partie>
                        <partie>
                            <licence>xxx</licence>
                            <advlic>xxx</advlic>
                            <vd>xxx</vd>
                            <numjourn>xxx</numjourn>
                            <codechamp>xxx</codechamp>
                            <date>xxx</date>
                            <advsexe>xxx</advsexe>
                            <advnompre>xxx</advnompre>
                            <pointres>xxx</pointres>
                            <coefchamp>xxx</coefchamp>
                            <advclaof>xxx</advclaof>
                            <idpartie>xxx</idpartie>
                        </partie>
                        <partie>
                            <licence>xxx</licence>
                            <advlic>xxx</advlic>
                            <vd>xxx</vd>
                            <numjourn>xxx</numjourn>
                            <codechamp>xxx</codechamp>
                            <date>xxx</date>
                            <advsexe>xxx</advsexe>
                            <advnompre>xxx</advnompre>
                            <pointres>xxx</pointres>
                            <coefchamp>xxx</coefchamp>
                            <advclaof>xxx</advclaof>
                            <idpartie>xxx</idpartie>
                        </partie>
                        <partie>
                            <licence>xxx</licence>
                            <advlic>xxx</advlic>
                            <vd>xxx</vd>
                            <numjourn>xxx</numjourn>
                            <codechamp>xxx</codechamp>
                            <date>xxx</date>
                            <advsexe>xxx</advsexe>
                            <advnompre>xxx</advnompre>
                            <pointres>xxx</pointres>
                            <coefchamp>xxx</coefchamp>
                            <advclaof>xxx</advclaof>
                            <idpartie>xxx</idpartie>
                        </partie>
                        <partie>
                            <licence>xxx</licence>
                            <advlic>xxx</advlic>
                            <vd>xxx</vd>
                            <numjourn>xxx</numjourn>
                            <codechamp>xxx</codechamp>
                            <date>xxx</date>
                            <advsexe>xxx</advsexe>
                            <advnompre>xxx</advnompre>
                            <pointres>xxx</pointres>
                            <coefchamp>xxx</coefchamp>
                            <advclaof>xxx</advclaof>
                            <idpartie>xxx</idpartie>
                        </partie>
                    </liste>";

        $arrayGames = $this->game::format($output);

        $this->assertInternalType('array', $arrayGames);
        $this->assertTrue(is_array($arrayGames));
        $this->assertEquals(14, count($arrayGames));
        $this->assertArrayHasKey(0, $arrayGames);

        $game = $arrayGames[0];
        $this->assertObjectHasAttribute('licence', $game);
        $this->assertObjectHasAttribute('advlic', $game);
        $this->assertObjectHasAttribute('vd', $game);
        $this->assertObjectHasAttribute('numjourn', $game);
        $this->assertObjectHasAttribute('codechamp', $game);
        $this->assertObjectHasAttribute('date', $game);
        $this->assertObjectHasAttribute('advsexe', $game);
        $this->assertObjectHasAttribute('advnompre', $game);
        $this->assertObjectHasAttribute('pointres', $game);
        $this->assertObjectHasAttribute('coefchamp', $game);
        $this->assertObjectHasAttribute('advclaof', $game);
        $this->assertObjectHasAttribute('idpartie', $game);

        $this->assertEquals("xxxL", $game->licence);
        $this->assertEquals("xxxA", $game->advlic);
        $this->assertEquals("xxxV", $game->vd);
        $this->assertEquals("xxxN", $game->numjourn);
        $this->assertEquals("xxxC", $game->codechamp);
        $this->assertEquals("xxxD", $game->date);
        $this->assertEquals("xxxAA", $game->advsexe);
        $this->assertEquals("xxxAAA", $game->advnompre);
        $this->assertEquals("xxxP", $game->pointres);
        $this->assertEquals("xxxCC", $game->coefchamp);
        $this->assertEquals("xxxAAAA", $game->advclaof);
        $this->assertEquals("xxxI", $game->idpartie);
    }

    public function testFormatUniqueSucceed()
    {
        $output = "<liste>
                        <partie>
                            <licence>xxxL</licence>
                            <advlic>xxxA</advlic>
                            <vd>xxxV</vd>
                            <numjourn>xxxN</numjourn>
                            <codechamp>xxxC</codechamp>
                            <date>xxxD</date>
                            <advsexe>xxxAA</advsexe>
                            <advnompre>xxxAAA</advnompre>
                            <pointres>xxxP</pointres>
                            <coefchamp>xxxCC</coefchamp>
                            <advclaof>xxxAAAA</advclaof>
                            <idpartie>xxxI</idpartie>
                        </partie>
                    </liste>";

        $arrayGames = $this->game::format($output);

        $this->assertInternalType('array', $arrayGames);
        $this->assertTrue(is_array($arrayGames));
        $this->assertEquals(1, count($arrayGames));
        $this->assertArrayHasKey(0, $arrayGames);

        $game = $arrayGames[0];
        $this->assertObjectHasAttribute('licence', $game);
        $this->assertObjectHasAttribute('advlic', $game);
        $this->assertObjectHasAttribute('vd', $game);
        $this->assertObjectHasAttribute('numjourn', $game);
        $this->assertObjectHasAttribute('codechamp', $game);
        $this->assertObjectHasAttribute('date', $game);
        $this->assertObjectHasAttribute('advsexe', $game);
        $this->assertObjectHasAttribute('advnompre', $game);
        $this->assertObjectHasAttribute('pointres', $game);
        $this->assertObjectHasAttribute('coefchamp', $game);
        $this->assertObjectHasAttribute('advclaof', $game);
        $this->assertObjectHasAttribute('idpartie', $game);

        $this->assertEquals("xxxL", $game->licence);
        $this->assertEquals("xxxA", $game->advlic);
        $this->assertEquals("xxxV", $game->vd);
        $this->assertEquals("xxxN", $game->numjourn);
        $this->assertEquals("xxxC", $game->codechamp);
        $this->assertEquals("xxxD", $game->date);
        $this->assertEquals("xxxAA", $game->advsexe);
        $this->assertEquals("xxxAAA", $game->advnompre);
        $this->assertEquals("xxxP", $game->pointres);
        $this->assertEquals("xxxCC", $game->coefchamp);
        $this->assertEquals("xxxAAAA", $game->advclaof);
        $this->assertEquals("xxxI", $game->idpartie);
    }

    /**
     * @expectedException \Exception
     */
    public function testFormatFailure()
    {
        $output = "<partie>
                        <licence>xxxL</licence>
                        <advlic>xxxA</advlic>
                        <vd>xxxV</vd>
                        <numjourn>xxxN</numjourn>
                        <codechamp>xxxC</codechamp>
                        <date>xxxD</date>
                        <advsexe>xxxAA</advsexe>
                        <advnompre>xxxAAA</advnompre>
                        <pointres>xxxP</pointres>
                        <coefchamp>xxxCC</coefchamp>
                        <advclaof>xxxAAAA</advclaof>
                        <idpartie>xxxI</idpartie>
                    </partie>
                    <partie>
                        <licence>xxx</licence>
                        <advlic>xxx</advlic>
                        <vd>xxx</vd>
                        <numjourn>xxx</numjourn>
                        <codechamp>xxx</codechamp>
                        <date>xxx</date>
                        <advsexe>xxx</advsexe>
                        <advnompre>xxx</advnompre>
                        <pointres>xxx</pointres>
                        <coefchamp>xxx</coefchamp>
                        <advclaof>xxx</advclaof>
                        <idpartie>xxx</idpartie>
                    </partie>
                    <partie>
                        <licence>xxx</licence>
                        <advlic>xxx</advlic>
                        <vd>xxx</vd>
                        <numjourn>xxx</numjourn>
                        <codechamp>xxx</codechamp>
                        <date>xxx</date>
                        <advsexe>xxx</advsexe>
                        <advnompre>xxx</advnompre>
                        <pointres>xxx</pointres>
                        <coefchamp>xxx</coefchamp>
                        <advclaof>xxx</advclaof>
                        <idpartie>xxx</idpartie>
                    </partie>
                    <partie>
                        <licence>xxx</licence>
                        <advlic>xxx</advlic>
                        <vd>xxx</vd>
                        <numjourn>xxx</numjourn>
                        <codechamp>xxx</codechamp>
                        <date>xxx</date>
                        <advsexe>xxx</advsexe>
                        <advnompre>xxx</advnompre>
                        <pointres>xxx</pointres>
                        <coefchamp>xxx</coefchamp>
                        <advclaof>xxx</advclaof>
                        <idpartie>xxx</idpartie>
                    </partie>
                    <partie>
                        <licence>xxx</licence>
                        <advlic>xxx</advlic>
                        <vd>xxx</vd>
                        <numjourn>xxx</numjourn>
                        <codechamp>xxx</codechamp>
                        <date>xxx</date>
                        <advsexe>xxx</advsexe>
                        <advnompre>xxx</advnompre>
                        <pointres>xxx</pointres>
                        <coefchamp>xxx</coefchamp>
                        <advclaof>xxx</advclaof>
                        <idpartie>xxx</idpartie>
                    </partie>
                    <partie>
                        <licence>xxx</licence>
                        <advlic>xxx</advlic>
                        <vd>xxx</vd>
                        <numjourn>xxx</numjourn>
                        <codechamp>xxx</codechamp>
                        <date>xxx</date>
                        <advsexe>xxx</advsexe>
                        <advnompre>xxx</advnompre>
                        <pointres>xxx</pointres>
                        <coefchamp>xxx</coefchamp>
                        <advclaof>xxx</advclaof>
                        <idpartie>xxx</idpartie>
                    </partie>
                    <partie>
                        <licence>xxx</licence>
                        <advlic>xxx</advlic>
                        <vd>xxx</vd>
                        <numjourn>xxx</numjourn>
                        <codechamp>xxx</codechamp>
                        <date>xxx</date>
                        <advsexe>xxx</advsexe>
                        <advnompre>xxx</advnompre>
                        <pointres>xxx</pointres>
                        <coefchamp>xxx</coefchamp>
                        <advclaof>xxx</advclaof>
                        <idpartie>xxx</idpartie>
                    </partie>
                    <partie>
                        <licence>xxx</licence>
                        <advlic>xxx</advlic>
                        <vd>xxx</vd>
                        <numjourn>xxx</numjourn>
                        <codechamp>xxx</codechamp>
                        <date>xxx</date>
                        <advsexe>xxx</advsexe>
                        <advnompre>xxx</advnompre>
                        <pointres>xxx</pointres>
                        <coefchamp>xxx</coefchamp>
                        <advclaof>xxx</advclaof>
                        <idpartie>xxx</idpartie>
                    </partie>
                    <partie>
                        <licence>xxx</licence>
                        <advlic>xxx</advlic>
                        <vd>xxx</vd>
                        <numjourn>xxx</numjourn>
                        <codechamp>xxx</codechamp>
                        <date>xxx</date>
                        <advsexe>xxx</advsexe>
                        <advnompre>xxx</advnompre>
                        <pointres>xxx</pointres>
                        <coefchamp>xxx</coefchamp>
                        <advclaof>xxx</advclaof>
                        <idpartie>xxx</idpartie>
                    </partie>
                    <partie>
                        <licence>xxx</licence>
                        <advlic>xxx</advlic>
                        <vd>xxx</vd>
                        <numjourn>xxx</numjourn>
                        <codechamp>xxx</codechamp>
                        <date>xxx</date>
                        <advsexe>xxx</advsexe>
                        <advnompre>xxx</advnompre>
                        <pointres>xxx</pointres>
                        <coefchamp>xxx</coefchamp>
                        <advclaof>xxx</advclaof>
                        <idpartie>xxx</idpartie>
                    </partie>
                    <partie>
                        <licence>xxx</licence>
                        <advlic>xxx</advlic>
                        <vd>xxx</vd>
                        <numjourn>xxx</numjourn>
                        <codechamp>xxx</codechamp>
                        <date>xxx</date>
                        <advsexe>xxx</advsexe>
                        <advnompre>xxx</advnompre>
                        <pointres>xxx</pointres>
                        <coefchamp>xxx</coefchamp>
                        <advclaof>xxx</advclaof>
                        <idpartie>xxx</idpartie>
                    </partie>
                    <partie>
                        <licence>xxx</licence>
                        <advlic>xxx</advlic>
                        <vd>xxx</vd>
                        <numjourn>xxx</numjourn>
                        <codechamp>xxx</codechamp>
                        <date>xxx</date>
                        <advsexe>xxx</advsexe>
                        <advnompre>xxx</advnompre>
                        <pointres>xxx</pointres>
                        <coefchamp>xxx</coefchamp>
                        <advclaof>xxx</advclaof>
                        <idpartie>xxx</idpartie>
                    </partie>
                    <partie>
                        <licence>xxx</licence>
                        <advlic>xxx</advlic>
                        <vd>xxx</vd>
                        <numjourn>xxx</numjourn>
                        <codechamp>xxx</codechamp>
                        <date>xxx</date>
                        <advsexe>xxx</advsexe>
                        <advnompre>xxx</advnompre>
                        <pointres>xxx</pointres>
                        <coefchamp>xxx</coefchamp>
                        <advclaof>xxx</advclaof>
                        <idpartie>xxx</idpartie>
                    </partie>
                    <partie>
                        <licence>xxx</licence>
                        <advlic>xxx</advlic>
                        <vd>xxx</vd>
                        <numjourn>xxx</numjourn>
                        <codechamp>xxx</codechamp>
                        <date>xxx</date>
                        <advsexe>xxx</advsexe>
                        <advnompre>xxx</advnompre>
                        <pointres>xxx</pointres>
                        <coefchamp>xxx</coefchamp>
                        <advclaof>xxx</advclaof>
                        <idpartie>xxx</idpartie>
                    </partie>";

        $arrayGames = $this->game::format($output);
    }

    public function testFormatUniqueFailure()
    {
        $output = "<partie>
                        <licence>xxxL</licence>
                        <advlic>xxxA</advlic>
                        <vd>xxxV</vd>
                        <numjourn>xxxN</numjourn>
                        <codechamp>xxxC</codechamp>
                        <date>xxxD</date>
                        <advsexe>xxxAA</advsexe>
                        <advnompre>xxxAAA</advnompre>
                        <pointres>xxxP</pointres>
                        <coefchamp>xxxCC</coefchamp>
                        <advclaof>xxxAAAA</advclaof>
                        <idpartie>xxxI</idpartie>
                    </partie>";

        $arrayGames = $this->game::format($output);

        $this->assertInternalType('array', $arrayGames);
        $this->assertTrue(is_array($arrayGames));
        $this->assertEquals(0, count($arrayGames));
        $this->assertArrayNotHasKey(0, $arrayGames);
    }

    public function setUp()
    {
        $this->game = new Game();
        parent::setUp(); // TODO: Change the autogenerated stub
    }
}