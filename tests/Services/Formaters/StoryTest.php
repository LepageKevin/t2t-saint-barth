<?php

namespace App\Test\Services\Formaters;

use App\Services\Formaters\Story;
use PHPUnit\Framework\TestCase;

class StoryTest extends TestCase {
    private $story;

    public function testFormatSucceed()
    {
        $output = "<liste>
                        <news>
                            <date>xxxD</date>
                            <titre>xxxT</titre>
                            <description>xxxD</description>
                            <url>xxxU</url>
                            <photo>xxxP</photo>
                            <categorie>xxxC</categorie>
                        </news>
                        <news>
                            <date>xxx</date>
                            <titre>xxx</titre>
                            <description>xxx</description>
                            <url>xxx</url>
                            <photo>xxx</photo>
                            <categorie>xxx</categorie>
                        </news>
                    </liste>";

        $arrayStories = $this->story::format($output);

        $this->assertInternalType('array', $arrayStories);
        $this->assertTrue(is_array($arrayStories));
        $this->assertEquals(2, count($arrayStories));
        $this->assertArrayHasKey(0, $arrayStories);

        $story = $arrayStories[0];
        $this->assertObjectHasAttribute('date', $story);
        $this->assertObjectHasAttribute('titre', $story);
        $this->assertObjectHasAttribute('description', $story);
        $this->assertObjectHasAttribute('url', $story);
        $this->assertObjectHasAttribute('photo', $story);
        $this->assertObjectHasAttribute('categorie', $story);

        $this->assertEquals("xxxD", $story->date);
        $this->assertEquals("xxxT", $story->titre);
        $this->assertEquals("xxxD", $story->description);
        $this->assertEquals("xxxU", $story->url);
        $this->assertEquals("xxxP", $story->photo);
        $this->assertEquals("xxxC", $story->categorie);
    }

    public function testFormatUniqueSucceed()
    {
        $output = "<liste>
                        <news>
                            <date>xxxD</date>
                            <titre>xxxT</titre>
                            <description>xxxD</description>
                            <url>xxxU</url>
                            <photo>xxxP</photo>
                            <categorie>xxxC</categorie>
                        </news>
                    </liste>";

        $arrayStories = $this->story::format($output);

        $this->assertInternalType('array', $arrayStories);
        $this->assertTrue(is_array($arrayStories));
        $this->assertEquals(1, count($arrayStories));
        $this->assertArrayHasKey(0, $arrayStories);

        $story = $arrayStories[0];
        $this->assertObjectHasAttribute('date', $story);
        $this->assertObjectHasAttribute('titre', $story);
        $this->assertObjectHasAttribute('description', $story);
        $this->assertObjectHasAttribute('url', $story);
        $this->assertObjectHasAttribute('photo', $story);
        $this->assertObjectHasAttribute('categorie', $story);

        $this->assertEquals("xxxD", $story->date);
        $this->assertEquals("xxxT", $story->titre);
        $this->assertEquals("xxxD", $story->description);
        $this->assertEquals("xxxU", $story->url);
        $this->assertEquals("xxxP", $story->photo);
        $this->assertEquals("xxxC", $story->categorie);
    }

    /**
     * @expectedException \Exception
     */
    public function testFormatFailure()
    {
        $output = "<news>
                        <date>xxxD</date>
                        <titre>xxxT</titre>
                        <description>xxxD</description>
                        <url>xxxU</url>
                        <photo>xxxP</photo>
                        <categorie>xxxC</categorie>
                    </news>
                    <news>
                        <date>xxx</date>
                        <titre>xxx</titre>
                        <description>xxx</description>
                        <url>xxx</url>
                        <photo>xxx</photo>
                        <categorie>xxx</categorie>
                    </news>";

        $arrayStories = $this->story::format($output);
    }

    public function testFormatUniqueFailure()
    {
        $output = "<news>
                        <date>xxxD</date>
                        <titre>xxxT</titre>
                        <description>xxxD</description>
                        <url>xxxU</url>
                        <photo>xxxP</photo>
                        <categorie>xxxC</categorie>
                    </news>";

        $arrayStories = $this->story::format($output);

        $this->assertInternalType('array', $arrayStories);
        $this->assertTrue(is_array($arrayStories));
        $this->assertEquals(0, count($arrayStories));
        $this->assertArrayNotHasKey(0, $arrayStories);
    }

    public function setUp()
    {
        $this->story = new Story();
        parent::setUp(); // TODO: Change the autogenerated stub
    }
}