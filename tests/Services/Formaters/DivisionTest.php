<?php

namespace App\Test\Services\Formaters;

use App\Services\Formaters\Division;
use PHPUnit\Framework\TestCase;

class DivisionTest extends TestCase {
    private $division;

    public function testFormatSucceed()
    {
        $output = "<liste>
                        <division>
                            <iddivision>xxxD</iddivision>
                            <libelle>xxxL</libelle>
                        </division>
                        <division>
                            <iddivision>xxx</iddivision>
                            <libelle>xxx</libelle>
                        </division>
                        <division>
                            <iddivision>xxx</iddivision>
                            <libelle>xxx</libelle>
                        </division>
                        <division>
                            <iddivision>xxx</iddivision>
                            <libelle>xxx</libelle>
                        </division>
                        <division>
                            <iddivision>xxx</iddivision>
                            <libelle>xxx</libelle>
                        </division>
                        <division>
                            <iddivision>xxx</iddivision>
                            <libelle>xxx</libelle>
                        </division>
                        <division>
                            <iddivision>xxx</iddivision>
                            <libelle>xxx</libelle>
                        </division>
                        <division>
                            <iddivision>xxx</iddivision>
                            <libelle>xxx</libelle>
                        </division>
                    </liste>";

        $arrayDivisions = $this->division::format($output);

        $this->assertInternalType('array', $arrayDivisions);
        $this->assertTrue(is_array($arrayDivisions));
        $this->assertEquals(8, count($arrayDivisions));
        $this->assertArrayHasKey(0, $arrayDivisions);

        $division = $arrayDivisions[0];
        $this->assertObjectHasAttribute('iddivision', $division);
        $this->assertObjectHasAttribute('libelle', $division);

        $this->assertEquals("xxxD", $division->iddivision);
        $this->assertEquals("xxxL", $division->libelle);
    }

    public function testFormatUniqueSucceed()
    {
        $output = "<liste>
                        <division>
                            <iddivision>xxxD</iddivision>
                            <libelle>xxxL</libelle>
                        </division>
                    </liste>";

        $arrayDivisions = $this->division::format($output);

        $this->assertInternalType('array', $arrayDivisions);
        $this->assertTrue(is_array($arrayDivisions));
        $this->assertEquals(1, count($arrayDivisions));
        $this->assertArrayHasKey(0, $arrayDivisions);

        $division = $arrayDivisions[0];
        $this->assertObjectHasAttribute('iddivision', $division);
        $this->assertObjectHasAttribute('libelle', $division);

        $this->assertEquals("xxxD", $division->iddivision);
        $this->assertEquals("xxxL", $division->libelle);
    }

    /**
     * @expectedException \Exception
     */
    public function testFormatFailure()
    {
        $output = "<division>
                        <iddivision>xxx</iddivision>
                        <libelle>xxx</libelle>
                    </division>
                    <division>
                        <iddivision>xxx</iddivision>
                        <libelle>xxx</libelle>
                    </division>
                    <division>
                        <iddivision>xxx</iddivision>
                        <libelle>xxx</libelle>
                    </division>
                    <division>
                        <iddivision>xxx</iddivision>
                        <libelle>xxx</libelle>
                    </division>
                    <division>
                        <iddivision>xxx</iddivision>
                        <libelle>xxx</libelle>
                    </division>
                    <division>
                        <iddivision>xxx</iddivision>
                        <libelle>xxx</libelle>
                    </division>
                    <division>
                        <iddivision>xxx</iddivision>
                        <libelle>xxx</libelle>
                    </division>
                    <division>
                        <iddivision>xxx</iddivision>
                        <libelle>xxx</libelle>
                    </division>";

        $arrayDivisions = $this->division::format($output);
    }

    public function testFormatUniqueFailure()
    {
        $output = "<division>
                        <iddivision>xxx</iddivision>
                        <libelle>xxx</libelle>
                    </division>";

        $arrayDivisions = $this->division::format($output);

        $this->assertInternalType('array', $arrayDivisions);
        $this->assertTrue(is_array($arrayDivisions));
        $this->assertEquals(0, count($arrayDivisions));
        $this->assertArrayNotHasKey(0, $arrayDivisions);
    }

    public function setUp()
    {
        $this->division = new Division();
        parent::setUp(); // TODO: Change the autogenerated stub
    }
}