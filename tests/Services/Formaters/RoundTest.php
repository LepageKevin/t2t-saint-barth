<?php

namespace App\Test\Services\Formaters;

use App\Services\Formaters\Round;
use PHPUnit\Framework\TestCase;

class RoundTest extends TestCase {
    private $round;

    public function testFormatSucceed()
    {
        $output = "<liste>
                        <tour>
                            <libelle>xxxL</libelle>
                            <equa>xxxE</equa>
                            <equb>xxxEE</equb>
                            <scorea>xxxS</scorea>
                            <scoreb>xxxSS</scoreb>
                            <lien>xxxLL</lien>
                            <dateprevue>xxxD</dateprevue>
                            <datereelle>xxxDD</datereelle>
                        </tour>
                        <tour>
                            <libelle>xxx</libelle>
                            <equa>xxx</equa>
                            <equb>xxx</equb>
                            <scorea>xxx</scorea>
                            <scoreb>xxx</scoreb>
                            <lien>xxx</lien>
                            <dateprevue>xxx</dateprevue>
                            <datereelle>xxx</datereelle>
                        </tour>
                        <tour>
                            <libelle>xxx</libelle>
                            <equa>xxx</equa>
                            <equb>xxx</equb>
                            <scorea>xxx</scorea>
                            <scoreb>xxx</scoreb>
                            <lien>xxx</lien>
                            <dateprevue>xxx</dateprevue>
                            <datereelle>xxx</datereelle>
                        </tour>
                        <tour>
                            <libelle>xxx</libelle>
                            <equa>xxx</equa>
                            <equb>xxx</equb>
                            <scorea>xxx</scorea>
                            <scoreb>xxx</scoreb>
                            <lien>xxx</lien>
                            <dateprevue>xxx</dateprevue>
                            <datereelle>xxx</datereelle>
                        </tour>
                        <tour>
                            <libelle>xxx</libelle>
                            <equa>xxx</equa>
                            <equb>xxx</equb>
                            <scorea>xxx</scorea>
                            <scoreb>xxx</scoreb>
                            <lien>xxx</lien>
                            <dateprevue>xxx</dateprevue>
                            <datereelle>xxx</datereelle>
                        </tour>
                    </liste>";

        $arrayRounds = $this->round::format($output);

        $this->assertInternalType('array', $arrayRounds);
        $this->assertTrue(is_array($arrayRounds));
        $this->assertEquals(5, count($arrayRounds));
        $this->assertArrayHasKey(0, $arrayRounds);

        $round = $arrayRounds[0];
        $this->assertObjectHasAttribute('libelle', $round);
        $this->assertObjectHasAttribute('equa', $round);
        $this->assertObjectHasAttribute('equb', $round);
        $this->assertObjectHasAttribute('scorea', $round);
        $this->assertObjectHasAttribute('scoreb', $round);
        $this->assertObjectHasAttribute('lien', $round);
        $this->assertObjectHasAttribute('dateprevue', $round);
        $this->assertObjectHasAttribute('datereelle', $round);

        $this->assertEquals("xxxL", $round->libelle);
        $this->assertEquals("xxxE", $round->equa);
        $this->assertEquals("xxxEE", $round->equb);
        $this->assertEquals("xxxS", $round->scorea);
        $this->assertEquals("xxxSS", $round->scoreb);
        $this->assertEquals("xxxLL", $round->lien);
        $this->assertEquals("xxxD", $round->dateprevue);
        $this->assertEquals("xxxDD", $round->datereelle);
    }

    public function testFormatUniqueSucceed()
    {
        $output = "<liste>
                        <tour>
                            <libelle>xxxL</libelle>
                            <equa>xxxE</equa>
                            <equb>xxxEE</equb>
                            <scorea>xxxS</scorea>
                            <scoreb>xxxSS</scoreb>
                            <lien>xxxLL</lien>
                            <dateprevue>xxxD</dateprevue>
                            <datereelle>xxxDD</datereelle>
                        </tour>
                    </liste>";

        $arrayRounds = $this->round::format($output);

        $this->assertInternalType('array', $arrayRounds);
        $this->assertTrue(is_array($arrayRounds));
        $this->assertEquals(1, count($arrayRounds));
        $this->assertArrayHasKey(0, $arrayRounds);

        $round = $arrayRounds[0];
        $this->assertObjectHasAttribute('libelle', $round);
        $this->assertObjectHasAttribute('equa', $round);
        $this->assertObjectHasAttribute('equb', $round);
        $this->assertObjectHasAttribute('scorea', $round);
        $this->assertObjectHasAttribute('scoreb', $round);
        $this->assertObjectHasAttribute('lien', $round);
        $this->assertObjectHasAttribute('dateprevue', $round);
        $this->assertObjectHasAttribute('datereelle', $round);

        $this->assertEquals("xxxL", $round->libelle);
        $this->assertEquals("xxxE", $round->equa);
        $this->assertEquals("xxxEE", $round->equb);
        $this->assertEquals("xxxS", $round->scorea);
        $this->assertEquals("xxxSS", $round->scoreb);
        $this->assertEquals("xxxLL", $round->lien);
        $this->assertEquals("xxxD", $round->dateprevue);
        $this->assertEquals("xxxDD", $round->datereelle);
    }

    /**
     * @expectedException \Exception
     */
    public function testFormatFailure()
    {
        $output = "<tour>
                        <libelle>xxxL</libelle>
                        <equa>xxxE</equa>
                        <equb>xxxEE</equb>
                        <scorea>xxxS</scorea>
                        <scoreb>xxxSS</scoreb>
                        <lien>xxxLL</lien>
                        <dateprevue>xxxD</dateprevue>
                        <datereelle>xxxDD</datereelle>
                    </tour>
                    <tour>
                        <libelle>xxx</libelle>
                        <equa>xxx</equa>
                        <equb>xxx</equb>
                        <scorea>xxx</scorea>
                        <scoreb>xxx</scoreb>
                        <lien>xxx</lien>
                        <dateprevue>xxx</dateprevue>
                        <datereelle>xxx</datereelle>
                    </tour>
                    <tour>
                        <libelle>xxx</libelle>
                        <equa>xxx</equa>
                        <equb>xxx</equb>
                        <scorea>xxx</scorea>
                        <scoreb>xxx</scoreb>
                        <lien>xxx</lien>
                        <dateprevue>xxx</dateprevue>
                        <datereelle>xxx</datereelle>
                    </tour>
                    <tour>
                        <libelle>xxx</libelle>
                        <equa>xxx</equa>
                        <equb>xxx</equb>
                        <scorea>xxx</scorea>
                        <scoreb>xxx</scoreb>
                        <lien>xxx</lien>
                        <dateprevue>xxx</dateprevue>
                        <datereelle>xxx</datereelle>
                    </tour>
                    <tour>
                        <libelle>xxx</libelle>
                        <equa>xxx</equa>
                        <equb>xxx</equb>
                        <scorea>xxx</scorea>
                        <scoreb>xxx</scoreb>
                        <lien>xxx</lien>
                        <dateprevue>xxx</dateprevue>
                        <datereelle>xxx</datereelle>
                    </tour>";

        $arrayRounds = $this->round::format($output);
    }

    public function testFormatUniqueFailure()
    {
        $output = "<tour>
                        <libelle>xxxL</libelle>
                        <equa>xxxE</equa>
                        <equb>xxxEE</equb>
                        <scorea>xxxS</scorea>
                        <scoreb>xxxSS</scoreb>
                        <lien>xxxLL</lien>
                        <dateprevue>xxxD</dateprevue>
                        <datereelle>xxxDD</datereelle>
                    </tour>";

        $arrayRounds = $this->round::format($output);

        $this->assertInternalType('array', $arrayRounds);
        $this->assertTrue(is_array($arrayRounds));
        $this->assertEquals(0, count($arrayRounds));
        $this->assertArrayNotHasKey(0, $arrayRounds);
    }

    public function setUp()
    {
        $this->round = new Round();
        parent::setUp(); // TODO: Change the autogenerated stub
    }
}