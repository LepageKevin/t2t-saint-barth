<?php

namespace App\Test\Services\Formaters;

use App\Services\Formaters\Team;
use PHPUnit\Framework\TestCase;

class TeamTest extends TestCase {
    private $team;

    public function testFormatSucceed()
    {
        $output = "<liste>
                        <equipe>
                            <libequipe>xxxL</libequipe>
                            <libdivision>xxxLL</libdivision>
                            <liendivision>xxxLLL</liendivision>
                            <idepr>xxxI</idepr>
                            <libepr>xxxLLLL</libepr>
                        </equipe>
                        <equipe>
                            <libequipe>xxx</libequipe>
                            <libdivision>xxx</libdivision>
                            <liendivision>xxx</liendivision>
                            <idepr>xxx</idepr>
                            <libepr>xxx</libepr>
                        </equipe>
                        <equipe>
                            <libequipe>xxx</libequipe>
                            <libdivision>xxx</libdivision>
                            <liendivision>xxx</liendivision>
                            <idepr>xxx</idepr>
                            <libepr>xxx</libepr>
                        </equipe>
                        <equipe>
                            <libequipe>xxx</libequipe>
                            <libdivision>xxx</libdivision>
                            <liendivision>xxx</liendivision>
                            <idepr>xxx</idepr>
                            <libepr>xxx</libepr>
                        </equipe>
                        <equipe>
                            <libequipe>xxx</libequipe>
                            <libdivision>xxx</libdivision>
                            <liendivision>xxx</liendivision>
                            <idepr>xxx</idepr>
                            <libepr>xxx</libepr>
                        </equipe>
                    </liste>";

        $arrayTeams = $this->team::format($output);

        $this->assertInternalType('array', $arrayTeams);
        $this->assertTrue(is_array($arrayTeams));
        $this->assertEquals(5, count($arrayTeams));
        $this->assertArrayHasKey(0, $arrayTeams);

        $team = $arrayTeams[0];
        $this->assertObjectHasAttribute('libequipe', $team);
        $this->assertObjectHasAttribute('libdivision', $team);
        $this->assertObjectHasAttribute('liendivision', $team);
        $this->assertObjectHasAttribute('idepr', $team);
        $this->assertObjectHasAttribute('libepr', $team);

        $this->assertEquals("xxxL", $team->libequipe);
        $this->assertEquals("xxxLL", $team->libdivision);
        $this->assertEquals("xxxLLL", $team->liendivision);
        $this->assertEquals("xxxI", $team->idepr);
        $this->assertEquals("xxxLLLL", $team->libepr);
    }

    public function testFormatUniqueSucceed()
    {
        $output = "<liste>
                        <equipe>
                            <libequipe>xxxL</libequipe>
                            <libdivision>xxxLL</libdivision>
                            <liendivision>xxxLLL</liendivision>
                            <idepr>xxxI</idepr>
                            <libepr>xxxLLLL</libepr>
                        </equipe>
                    </liste>";

        $arrayTeams = $this->team::format($output);

        $this->assertInternalType('array', $arrayTeams);
        $this->assertTrue(is_array($arrayTeams));
        $this->assertEquals(1, count($arrayTeams));
        $this->assertArrayHasKey(0, $arrayTeams);

        $team = $arrayTeams[0];
        $this->assertObjectHasAttribute('libequipe', $team);
        $this->assertObjectHasAttribute('libdivision', $team);
        $this->assertObjectHasAttribute('liendivision', $team);
        $this->assertObjectHasAttribute('idepr', $team);
        $this->assertObjectHasAttribute('libepr', $team);

        $this->assertEquals("xxxL", $team->libequipe);
        $this->assertEquals("xxxLL", $team->libdivision);
        $this->assertEquals("xxxLLL", $team->liendivision);
        $this->assertEquals("xxxI", $team->idepr);
        $this->assertEquals("xxxLLLL", $team->libepr);
    }

    /**
     * @expectedException \Exception
     */
    public function testFormatFailure()
    {
        $output = "<equipe>
                        <libequipe>xxxL</libequipe>
                        <libdivision>xxxLL</libdivision>
                        <liendivision>xxxLLL</liendivision>
                        <idepr>xxxI</idepr>
                        <libepr>xxxLLLL</libepr>
                    </equipe>
                    <equipe>
                        <libequipe>xxx</libequipe>
                        <libdivision>xxx</libdivision>
                        <liendivision>xxx</liendivision>
                        <idepr>xxx</idepr>
                        <libepr>xxx</libepr>
                    </equipe>
                    <equipe>
                        <libequipe>xxx</libequipe>
                        <libdivision>xxx</libdivision>
                        <liendivision>xxx</liendivision>
                        <idepr>xxx</idepr>
                        <libepr>xxx</libepr>
                    </equipe>
                    <equipe>
                        <libequipe>xxx</libequipe>
                        <libdivision>xxx</libdivision>
                        <liendivision>xxx</liendivision>
                        <idepr>xxx</idepr>
                        <libepr>xxx</libepr>
                    </equipe>
                    <equipe>
                        <libequipe>xxx</libequipe>
                        <libdivision>xxx</libdivision>
                        <liendivision>xxx</liendivision>
                        <idepr>xxx</idepr>
                        <libepr>xxx</libepr>
                    </equipe>";

        $arrayTeams = $this->team::format($output);
    }

    public function testFormatUniqueFailure()
    {
        $output = "<equipe>
                        <libequipe>xxxL</libequipe>
                        <libdivision>xxxLL</libdivision>
                        <liendivision>xxxLLL</liendivision>
                        <idepr>xxxI</idepr>
                        <libepr>xxxLLLL</libepr>
                    </equipe>";

        $arrayTeams = $this->team::format($output);

        $this->assertInternalType('array', $arrayTeams);
        $this->assertTrue(is_array($arrayTeams));
        $this->assertEquals(0, count($arrayTeams));
        $this->assertArrayNotHasKey(0, $arrayTeams);
    }

    public function setUp()
    {
        $this->team = new Team();
        parent::setUp(); // TODO: Change the autogenerated stub
    }
}