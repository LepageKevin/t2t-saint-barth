<?php

namespace App\Test\Services\Formaters;

use App\Services\Formaters\Club;
use PHPUnit\Framework\TestCase;

class ClubTest extends TestCase {
    private $club;

    public function testFormatSucceed()
    {
        $output = "<liste>
                        <club>
                            <idclub>xxxI</idclub>
                            <numero>xxxN</numero>
                            <nom>xxxNN</nom>
                            <validation>xxxV</validation>
                            <typeclub>xxxT</typeclub>
                        </club>
                        <club>
                            <idclub>xxx</idclub>
                            <numero>xxx</numero>
                            <nom>xxx</nom>
                            <validation>xxx</validation>
                            <typeclub>xxx</typeclub>
                        </club>
                        <club>
                            <idclub>xxx</idclub>
                            <numero>xxx</numero>
                            <nom>xxx</nom>
                            <validation>xxx</validation>
                            <typeclub>xxx</typeclub>
                        </club>
                        <club>
                            <idclub>xxx</idclub>
                            <numero>xxx</numero>
                            <nom>xxx</nom>
                            <validation>xxx</validation>
                            <typeclub>xxx</typeclub>
                        </club>
                    </liste>";

        $arrayClubs = $this->club::format($output);

        $this->assertInternalType('array', $arrayClubs);
        $this->assertTrue(is_array($arrayClubs));
        $this->assertEquals(4, count($arrayClubs));
        $this->assertArrayHasKey(0, $arrayClubs);

        $club = $arrayClubs[0];
        $this->assertObjectHasAttribute('idclub', $club);
        $this->assertObjectHasAttribute('numero', $club);
        $this->assertObjectHasAttribute('nom', $club);
        $this->assertObjectHasAttribute('validation', $club);
        $this->assertObjectHasAttribute('typeclub', $club);

        $this->assertEquals("xxxI", $club->idclub);
        $this->assertEquals("xxxN", $club->numero);
        $this->assertEquals("xxxNN", $club->nom);
        $this->assertEquals("xxxV", $club->validation);
        $this->assertEquals("xxxT", $club->typeclub);
    }

    public function testFormatUniqueSucceed()
    {
        $output = "<liste>
                        <club>
                            <idclub>xxxI</idclub>
                            <numero>xxxN</numero>
                            <nom>xxxNN</nom>
                            <validation>xxxV</validation>
                            <typeclub>xxxT</typeclub>
                        </club>
                    </liste>";

        $arrayClubs = $this->club::format($output);

        $this->assertInternalType('array', $arrayClubs);
        $this->assertTrue(is_array($arrayClubs));
        $this->assertEquals(1, count($arrayClubs));
        $this->assertArrayHasKey(0, $arrayClubs);

        $club = $arrayClubs[0];
        $this->assertObjectHasAttribute('idclub', $club);
        $this->assertObjectHasAttribute('numero', $club);
        $this->assertObjectHasAttribute('nom', $club);
        $this->assertObjectHasAttribute('validation', $club);
        $this->assertObjectHasAttribute('typeclub', $club);

        $this->assertEquals("xxxI", $club->idclub);
        $this->assertEquals("xxxN", $club->numero);
        $this->assertEquals("xxxNN", $club->nom);
        $this->assertEquals("xxxV", $club->validation);
        $this->assertEquals("xxxT", $club->typeclub);
    }

    /**
     * @expectedException \Exception
     */
    public function testFormatFailure()
    {
        $output = "<club>
                        <idclub>xxx</idclub>
                        <numero>xxx</numero>
                        <nom>xxx</nom>
                        <validation>xxx</validation>
                        <typeclub>xxx</typeclub>
                    </club>
                    <club>
                        <idclub>xxx</idclub>
                        <numero>xxx</numero>
                        <nom>xxx</nom>
                        <validation>xxx</validation>
                        <typeclub>xxx</typeclub>
                    </club>
                    <club>
                        <idclub>xxx</idclub>
                        <numero>xxx</numero>
                        <nom>xxx</nom>
                        <validation>xxx</validation>
                        <typeclub>xxx</typeclub>
                    </club>
                    <club>
                        <idclub>xxx</idclub>
                        <numero>xxx</numero>
                        <nom>xxx</nom>
                        <validation>xxx</validation>
                        <typeclub>xxx</typeclub>
                    </club>";

        $arrayClubs = $this->club::format($output);
    }

    public function testFormatUniqueFailure()
    {
        $output = "<club>
                        <idclub>xxx</idclub>
                        <numero>xxx</numero>
                        <nom>xxx</nom>
                        <validation>xxx</validation>
                        <typeclub>xxx</typeclub>
                    </club>";

        $arrayClubs = $this->club::format($output);

        $this->assertInternalType('array', $arrayClubs);
        $this->assertTrue(is_array($arrayClubs));
        $this->assertEquals(0, count($arrayClubs));
        $this->assertArrayNotHasKey(0, $arrayClubs);
    }

    public function setUp()
    {
        $this->club = new Club();
        parent::setUp(); // TODO: Change the autogenerated stub
    }
}