<?php

namespace App\Test\Services\Formaters;

use App\Services\Formaters\Historic;
use PHPUnit\Framework\TestCase;

class HistoricTest extends TestCase {
    private $historic;

    public function testFormatSucceed()
    {
        $output = "<liste>
                        <histo>
                            <echelon>xxxE</echelon>
                            <place>xxxP</place>
                            <point>xxxPP</point>
                            <saison>xxxS</saison>
                            <phase>xxxPPP</phase>
                        </histo>
                        <histo>
                            <echelon>xxx</echelon>
                            <place>xxx</place>
                            <point>xxx</point>
                            <saison>xxx</saison>
                            <phase>xxx</phase>
                        </histo>
                        <histo>
                            <echelon>xxx</echelon>
                            <place>xxx</place>
                            <point>xxx</point>
                            <saison>xxx</saison>
                            <phase>xxx</phase>
                        </histo>
                        <histo>
                            <echelon>xxx</echelon>
                            <place>xxx</place>
                            <point>xxx</point>
                            <saison>xxx</saison>
                            <phase>xxx</phase>
                        </histo>
                        <histo>
                            <echelon>xxx</echelon>
                            <place>xxx</place>
                            <point>xxx</point>
                            <saison>xxx</saison>
                            <phase>xxx</phase>
                        </histo>
                        <histo>
                            <echelon>xxx</echelon>
                            <place>xxx</place>
                            <point>xxx</point>
                            <saison>xxx</saison>
                            <phase>xxx</phase>
                        </histo>
                        <histo>
                            <echelon>xxx</echelon>
                            <place>xxx</place>
                            <point>xxx</point>
                            <saison>xxx</saison>
                            <phase>xxx</phase>
                        </histo>
                    </liste>";

        $arrayHistorics = $this->historic::format($output);

        $this->assertInternalType('array', $arrayHistorics);
        $this->assertTrue(is_array($arrayHistorics));
        $this->assertEquals(7, count($arrayHistorics));
        $this->assertArrayHasKey(0, $arrayHistorics);

        $historic = $arrayHistorics[0];
        $this->assertObjectHasAttribute('echelon', $historic);
        $this->assertObjectHasAttribute('place', $historic);
        $this->assertObjectHasAttribute('point', $historic);
        $this->assertObjectHasAttribute('saison', $historic);
        $this->assertObjectHasAttribute('phase', $historic);

        $this->assertEquals("xxxE", $historic->echelon);
        $this->assertEquals("xxxP", $historic->place);
        $this->assertEquals("xxxPP", $historic->point);
        $this->assertEquals("xxxS", $historic->saison);
        $this->assertEquals("xxxPPP", $historic->phase);
    }

    public function testFormatUniqueSucceed()
    {
        $output = "<liste>
                        <histo>
                            <echelon>xxxE</echelon>
                            <place>xxxP</place>
                            <point>xxxPP</point>
                            <saison>xxxS</saison>
                            <phase>xxxPPP</phase>
                        </histo>
                    </liste>";

        $arrayHistorics = $this->historic::format($output);

        $this->assertInternalType('array', $arrayHistorics);
        $this->assertTrue(is_array($arrayHistorics));
        $this->assertEquals(1, count($arrayHistorics));
        $this->assertArrayHasKey(0, $arrayHistorics);

        $historic = $arrayHistorics[0];
        $this->assertObjectHasAttribute('echelon', $historic);
        $this->assertObjectHasAttribute('place', $historic);
        $this->assertObjectHasAttribute('point', $historic);
        $this->assertObjectHasAttribute('saison', $historic);
        $this->assertObjectHasAttribute('phase', $historic);

        $this->assertEquals("xxxE", $historic->echelon);
        $this->assertEquals("xxxP", $historic->place);
        $this->assertEquals("xxxPP", $historic->point);
        $this->assertEquals("xxxS", $historic->saison);
        $this->assertEquals("xxxPPP", $historic->phase);
    }

    /**
     * @expectedException \Exception
     */
    public function testFormatFailure()
    {
        $output = "<histo>
                        <echelon>xxxE</echelon>
                        <place>xxxP</place>
                        <point>xxxPP</point>
                        <saison>xxxS</saison>
                        <phase>xxxPPP</phase>
                    </histo>
                    <histo>
                        <echelon>xxx</echelon>
                        <place>xxx</place>
                        <point>xxx</point>
                        <saison>xxx</saison>
                        <phase>xxx</phase>
                    </histo>
                    <histo>
                        <echelon>xxx</echelon>
                        <place>xxx</place>
                        <point>xxx</point>
                        <saison>xxx</saison>
                        <phase>xxx</phase>
                    </histo>
                    <histo>
                        <echelon>xxx</echelon>
                        <place>xxx</place>
                        <point>xxx</point>
                        <saison>xxx</saison>
                        <phase>xxx</phase>
                    </histo>
                    <histo>
                        <echelon>xxx</echelon>
                        <place>xxx</place>
                        <point>xxx</point>
                        <saison>xxx</saison>
                        <phase>xxx</phase>
                    </histo>
                    <histo>
                        <echelon>xxx</echelon>
                        <place>xxx</place>
                        <point>xxx</point>
                        <saison>xxx</saison>
                        <phase>xxx</phase>
                    </histo>
                    <histo>
                        <echelon>xxx</echelon>
                        <place>xxx</place>
                        <point>xxx</point>
                        <saison>xxx</saison>
                        <phase>xxx</phase>
                    </histo>";

        $arrayHistorics = $this->historic::format($output);
    }

    public function testFormatUniqueFailure()
    {
        $output = "<histo>
                        <echelon>xxxE</echelon>
                        <place>xxxP</place>
                        <point>xxxPP</point>
                        <saison>xxxS</saison>
                        <phase>xxxPPP</phase>
                    </histo>";

        $arrayHistorics = $this->historic::format($output);

        $this->assertInternalType('array', $arrayHistorics);
        $this->assertTrue(is_array($arrayHistorics));
        $this->assertEquals(0, count($arrayHistorics));
        $this->assertArrayNotHasKey(0, $arrayHistorics);
    }

    public function setUp()
    {
        $this->historic = new Historic();
        parent::setUp(); // TODO: Change the autogenerated stub
    }
}