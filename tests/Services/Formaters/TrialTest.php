<?php

namespace App\Test\Services\Formaters;

use App\Services\Formaters\Trial;
use PHPUnit\Framework\TestCase;

class TrialTest extends TestCase {
    private $trial;

    public function testFormatSucceed()
    {
        $output = "<liste>
                        <epreuve>
                            <idepreuve>xxxI</idepreuve>
                            <idorga>xxxII</idorga>
                            <libelle>xxxL</libelle>
                        </epreuve>
                        <epreuve>
                            <idepreuve>xxx</idepreuve>
                            <idorga>xxx</idorga>
                            <libelle>xxx</libelle>
                        </epreuve>
                        <epreuve>
                            <idepreuve>xxx</idepreuve>
                            <idorga>xxx</idorga>
                            <libelle>xxx</libelle>
                        </epreuve>
                        <epreuve>
                            <idepreuve>xxx</idepreuve>
                            <idorga>xxx</idorga>
                            <libelle>xxx</libelle>
                        </epreuve>
                        <epreuve>
                            <idepreuve>xxx</idepreuve>
                            <idorga>xxx</idorga>
                            <libelle>xxx</libelle>
                        </epreuve>
                        <epreuve>
                            <idepreuve>xxx</idepreuve>
                            <idorga>xxx</idorga>
                            <libelle>xxx</libelle>
                        </epreuve>
                    </liste>";

        $arrayTrials = $this->trial::format($output);

        $this->assertInternalType('array', $arrayTrials);
        $this->assertTrue(is_array($arrayTrials));
        $this->assertEquals(6, count($arrayTrials));
        $this->assertArrayHasKey(0, $arrayTrials);

        $trial = $arrayTrials[0];
        $this->assertObjectHasAttribute('idepreuve', $trial);
        $this->assertObjectHasAttribute('idorga', $trial);
        $this->assertObjectHasAttribute('libelle', $trial);

        $this->assertEquals("xxxI", $trial->idepreuve);
        $this->assertEquals("xxxII", $trial->idorga);
        $this->assertEquals("xxxL", $trial->libelle);
    }

    public function testFormatUniqueSucceed()
    {
        $output = "<liste>
                        <epreuve>
                            <idepreuve>xxxI</idepreuve>
                            <idorga>xxxII</idorga>
                            <libelle>xxxL</libelle>
                        </epreuve>
                    </liste>";

        $arrayTrials = $this->trial::format($output);

        $this->assertInternalType('array', $arrayTrials);
        $this->assertTrue(is_array($arrayTrials));
        $this->assertEquals(1, count($arrayTrials));
        $this->assertArrayHasKey(0, $arrayTrials);

        $trial = $arrayTrials[0];
        $this->assertObjectHasAttribute('idepreuve', $trial);
        $this->assertObjectHasAttribute('idorga', $trial);
        $this->assertObjectHasAttribute('libelle', $trial);

        $this->assertEquals("xxxI", $trial->idepreuve);
        $this->assertEquals("xxxII", $trial->idorga);
        $this->assertEquals("xxxL", $trial->libelle);
    }

    /**
     * @expectedException \Exception
     */
    public function testFormatFailure()
    {
        $output = "<epreuve>
                        <idepreuve>xxxI</idepreuve>
                        <idorga>xxxII</idorga>
                        <libelle>xxxL</libelle>
                    </epreuve>
                    <epreuve>
                        <idepreuve>xxx</idepreuve>
                        <idorga>xxx</idorga>
                        <libelle>xxx</libelle>
                    </epreuve>
                    <epreuve>
                        <idepreuve>xxx</idepreuve>
                        <idorga>xxx</idorga>
                        <libelle>xxx</libelle>
                    </epreuve>
                    <epreuve>
                        <idepreuve>xxx</idepreuve>
                        <idorga>xxx</idorga>
                        <libelle>xxx</libelle>
                    </epreuve>
                    <epreuve>
                        <idepreuve>xxx</idepreuve>
                        <idorga>xxx</idorga>
                        <libelle>xxx</libelle>
                    </epreuve>
                    <epreuve>
                        <idepreuve>xxx</idepreuve>
                        <idorga>xxx</idorga>
                        <libelle>xxx</libelle>
                        </epreuve>";

        $arrayTrials = $this->trial::format($output);
    }

    public function testFormatUniqueFailure()
    {
        $output = "<epreuve>
                        <idepreuve>xxxI</idepreuve>
                        <idorga>xxxII</idorga>
                        <libelle>xxxL</libelle>
                    </epreuve>";

        $arrayTrials = $this->trial::format($output);

        $this->assertInternalType('array', $arrayTrials);
        $this->assertTrue(is_array($arrayTrials));
        $this->assertEquals(0, count($arrayTrials));
        $this->assertArrayNotHasKey(0, $arrayTrials);
    }

    public function setUp()
    {
        $this->trial = new Trial();
        parent::setUp(); // TODO: Change the autogenerated stub
    }
}