<?php

namespace App\Test\Services\Formaters;

use App\Services\Formaters\Rank;
use PHPUnit\Framework\TestCase;

class RankTest extends TestCase {
    private $rank;

    public function testFormatSucceed()
    {
        $output = "<liste>
                        <classement>
                            <poule>xxxP</poule>
                            <clt>xxxC</clt>
                            <equipe>xxxE</equipe>
                            <joue>xxxJ</joue>
                            <pts>xxxP</pts>
                            <numero>xxxN</numero>
                            <totvic>xxxT</totvic>
                            <totdef>xxxTT</totdef>
                            <idequipe>xxxI</idequipe>
                            <idclub>xxxII</idclub>
                            <vic>xxxV</vic>
                            <def>xxxD</def>
                            <nul>xxxN</nul>
                            <pf>xxxPP</pf>
                            <pg>xxxPPP</pg>
                            <pp>xxxPPPP</pp>
                        </classement>
                        <classement>
                            <poule>xxx</poule>
                            <clt>xxx</clt>
                            <equipe>xxx</equipe>
                            <joue>xxx</joue>
                            <pts>xxx</pts>
                            <numero>xxx</numero>
                            <totvic>xxx</totvic>
                            <totdef>xxx</totdef>
                            <idequipe>xxx</idequipe>
                            <idclub>xxx</idclub>
                            <vic>xxx</vic>
                            <def>xxx</def>
                            <nul>xxx</nul>
                            <pf>xxx</pf>
                            <pg>xxx</pg>
                            <pp>xxx</pp>
                        </classement>
                        <classement>
                            <poule>xxx</poule>
                            <clt>xxx</clt>
                            <equipe>xxx</equipe>
                            <joue>xxx</joue>
                            <pts>xxx</pts>
                            <numero>xxx</numero>
                            <totvic>xxx</totvic>
                            <totdef>xxx</totdef>
                            <idequipe>xxx</idequipe>
                            <idclub>xxx</idclub>
                            <vic>xxx</vic>
                            <def>xxx</def>
                            <nul>xxx</nul>
                            <pf>xxx</pf>
                            <pg>xxx</pg>
                            <pp>xxx</pp>
                        </classement>
                    </liste>";

        $arrayRanks = $this->rank::format($output);

        $this->assertInternalType('array', $arrayRanks);
        $this->assertTrue(is_array($arrayRanks));
        $this->assertEquals(3, count($arrayRanks));
        $this->assertArrayHasKey(0, $arrayRanks);

        $rank = $arrayRanks[0];
        $this->assertObjectHasAttribute('poule', $rank);
        $this->assertObjectHasAttribute('clt', $rank);
        $this->assertObjectHasAttribute('equipe', $rank);
        $this->assertObjectHasAttribute('joue', $rank);
        $this->assertObjectHasAttribute('pts', $rank);
        $this->assertObjectHasAttribute('numero', $rank);
        $this->assertObjectHasAttribute('totvic', $rank);
        $this->assertObjectHasAttribute('totdef', $rank);
        $this->assertObjectHasAttribute('idequipe', $rank);
        $this->assertObjectHasAttribute('idclub', $rank);
        $this->assertObjectHasAttribute('vic', $rank);
        $this->assertObjectHasAttribute('def', $rank);
        $this->assertObjectHasAttribute('nul', $rank);
        $this->assertObjectHasAttribute('pf', $rank);
        $this->assertObjectHasAttribute('pg', $rank);
        $this->assertObjectHasAttribute('pp', $rank);

        $this->assertEquals("xxxP", $rank->poule);
        $this->assertEquals("xxxC", $rank->clt);
        $this->assertEquals("xxxE", $rank->equipe);
        $this->assertEquals("xxxJ", $rank->joue);
        $this->assertEquals("xxxP", $rank->pts);
        $this->assertEquals("xxxN", $rank->numero);
        $this->assertEquals("xxxT", $rank->totvic);
        $this->assertEquals("xxxTT", $rank->totdef);
        $this->assertEquals("xxxI", $rank->idequipe);
        $this->assertEquals("xxxII", $rank->idclub);
        $this->assertEquals("xxxV", $rank->vic);
        $this->assertEquals("xxxD", $rank->def);
        $this->assertEquals("xxxN", $rank->nul);
        $this->assertEquals("xxxPP", $rank->pf);
        $this->assertEquals("xxxPPP", $rank->pg);
        $this->assertEquals("xxxPPPP", $rank->pp);
    }

    public function testFormatUniqueSucceed()
    {
        $output = "<liste>
                        <classement>
                            <poule>xxxP</poule>
                            <clt>xxxC</clt>
                            <equipe>xxxE</equipe>
                            <joue>xxxJ</joue>
                            <pts>xxxP</pts>
                            <numero>xxxN</numero>
                            <totvic>xxxT</totvic>
                            <totdef>xxxTT</totdef>
                            <idequipe>xxxI</idequipe>
                            <idclub>xxxII</idclub>
                            <vic>xxxV</vic>
                            <def>xxxD</def>
                            <nul>xxxN</nul>
                            <pf>xxxPP</pf>
                            <pg>xxxPPP</pg>
                            <pp>xxxPPPP</pp>
                        </classement>
                    </liste>";

        $arrayRanks = $this->rank::format($output);

        $this->assertInternalType('array', $arrayRanks);
        $this->assertTrue(is_array($arrayRanks));
        $this->assertEquals(1, count($arrayRanks));
        $this->assertArrayHasKey(0, $arrayRanks);

        $rank = $arrayRanks[0];
        $this->assertObjectHasAttribute('poule', $rank);
        $this->assertObjectHasAttribute('clt', $rank);
        $this->assertObjectHasAttribute('equipe', $rank);
        $this->assertObjectHasAttribute('joue', $rank);
        $this->assertObjectHasAttribute('pts', $rank);
        $this->assertObjectHasAttribute('numero', $rank);
        $this->assertObjectHasAttribute('totvic', $rank);
        $this->assertObjectHasAttribute('totdef', $rank);
        $this->assertObjectHasAttribute('idequipe', $rank);
        $this->assertObjectHasAttribute('idclub', $rank);
        $this->assertObjectHasAttribute('vic', $rank);
        $this->assertObjectHasAttribute('def', $rank);
        $this->assertObjectHasAttribute('nul', $rank);
        $this->assertObjectHasAttribute('pf', $rank);
        $this->assertObjectHasAttribute('pg', $rank);
        $this->assertObjectHasAttribute('pp', $rank);

        $this->assertEquals("xxxP", $rank->poule);
        $this->assertEquals("xxxC", $rank->clt);
        $this->assertEquals("xxxE", $rank->equipe);
        $this->assertEquals("xxxJ", $rank->joue);
        $this->assertEquals("xxxP", $rank->pts);
        $this->assertEquals("xxxN", $rank->numero);
        $this->assertEquals("xxxT", $rank->totvic);
        $this->assertEquals("xxxTT", $rank->totdef);
        $this->assertEquals("xxxI", $rank->idequipe);
        $this->assertEquals("xxxII", $rank->idclub);
        $this->assertEquals("xxxV", $rank->vic);
        $this->assertEquals("xxxD", $rank->def);
        $this->assertEquals("xxxN", $rank->nul);
        $this->assertEquals("xxxPP", $rank->pf);
        $this->assertEquals("xxxPPP", $rank->pg);
        $this->assertEquals("xxxPPPP", $rank->pp);
    }

    /**
     * @expectedException \Exception
     */
    public function testFormatFailure()
    {
        $output = "<classement>
                        <poule>xxxP</poule>
                        <clt>xxxC</clt>
                        <equipe>xxxE</equipe>
                        <joue>xxxJ</joue>
                        <pts>xxxP</pts>
                        <numero>xxxN</numero>
                        <totvic>xxxT</totvic>
                        <totdef>xxxTT</totdef>
                        <idequipe>xxxI</idequipe>
                        <idclub>xxxII</idclub>
                        <vic>xxxV</vic>
                        <def>xxxD</def>
                        <nul>xxxN</nul>
                        <pf>xxxPP</pf>
                        <pg>xxxPPP</pg>
                        <pp>xxxPPPP</pp>
                    </classement>
                    <classement>
                        <poule>xxx</poule>
                        <clt>xxx</clt>
                        <equipe>xxx</equipe>
                        <joue>xxx</joue>
                        <pts>xxx</pts>
                        <numero>xxx</numero>
                        <totvic>xxx</totvic>
                        <totdef>xxx</totdef>
                        <idequipe>xxx</idequipe>
                        <idclub>xxx</idclub>
                        <vic>xxx</vic>
                        <def>xxx</def>
                        <nul>xxx</nul>
                        <pf>xxx</pf>
                        <pg>xxx</pg>
                        <pp>xxx</pp>
                    </classement>
                    <classement>
                        <poule>xxx</poule>
                        <clt>xxx</clt>
                        <equipe>xxx</equipe>
                        <joue>xxx</joue>
                        <pts>xxx</pts>
                        <numero>xxx</numero>
                        <totvic>xxx</totvic>
                        <totdef>xxx</totdef>
                        <idequipe>xxx</idequipe>
                        <idclub>xxx</idclub>
                        <vic>xxx</vic>
                        <def>xxx</def>
                        <nul>xxx</nul>
                        <pf>xxx</pf>
                        <pg>xxx</pg>
                        <pp>xxx</pp>
                    </classement>";

        $arrayRanks = $this->rank::format($output);
    }

    public function testFormatUniqueFailure()
    {
        $output = "<classement>
                        <poule>xxxP</poule>
                        <clt>xxxC</clt>
                        <equipe>xxxE</equipe>
                        <joue>xxxJ</joue>
                        <pts>xxxP</pts>
                        <numero>xxxN</numero>
                        <totvic>xxxT</totvic>
                        <totdef>xxxTT</totdef>
                        <idequipe>xxxI</idequipe>
                        <idclub>xxxII</idclub>
                        <vic>xxxV</vic>
                        <def>xxxD</def>
                        <nul>xxxN</nul>
                        <pf>xxxPP</pf>
                        <pg>xxxPPP</pg>
                        <pp>xxxPPPP</pp>
                    </classement>";

        $arrayRanks = $this->rank::format($output);

        $this->assertInternalType('array', $arrayRanks);
        $this->assertTrue(is_array($arrayRanks));
        $this->assertEquals(0, count($arrayRanks));
        $this->assertArrayNotHasKey(0, $arrayRanks);
    }

    public function setUp()
    {
        $this->rank = new Rank();
        parent::setUp(); // TODO: Change the autogenerated stub
    }
}