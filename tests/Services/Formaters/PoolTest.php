<?php

namespace App\Test\Services\Formaters;

use App\Services\Formaters\Pool;
use PHPUnit\Framework\TestCase;

class PoolTest extends TestCase {
    private $pool;

    public function testFormatSucceed()
    {
        $output = "<liste>
                        <poule>
                            <lien>xxxL</lien>
                            <libelle>xxxLL</libelle>
                        </poule>
                        <poule>
                            <lien>xxx</lien>
                            <libelle>xxx</libelle>
                        </poule>
                        <poule>
                            <lien>xxx</lien>
                            <libelle>xxx</libelle>
                        </poule>
                        <poule>
                            <lien>xxx</lien>
                            <libelle>xxx</libelle>
                        </poule>
                        <poule>
                            <lien>xxx</lien>
                            <libelle>xxx</libelle>
                        </poule>
                        <poule>
                            <lien>xxx</lien>
                            <libelle>xxx</libelle>
                        </poule>
                        <poule>
                            <lien>xxx</lien>
                            <libelle>xxx</libelle>
                        </poule>
                        <poule>
                            <lien>xxx</lien>
                            <libelle>xxx</libelle>
                        </poule>
                        <poule>
                            <lien>xxx</lien>
                            <libelle>xxx</libelle>
                        </poule>
                        <poule>
                            <lien>xxx</lien>
                            <libelle>xxx</libelle>
                        </poule>
                        <poule>
                            <lien>xxx</lien>
                            <libelle>xxx</libelle>
                        </poule>
                        <poule>
                            <lien>xxx</lien>
                            <libelle>xxx</libelle>
                        </poule>
                        <poule>
                            <lien>xxx</lien>
                            <libelle>xxx</libelle>
                        </poule>
                    </liste>";

        $arrayPools = $this->pool::format($output);

        $this->assertInternalType('array', $arrayPools);
        $this->assertTrue(is_array($arrayPools));
        $this->assertEquals(13, count($arrayPools));
        $this->assertArrayHasKey(0, $arrayPools);

        $pool = $arrayPools[0];
        $this->assertObjectHasAttribute('lien', $pool);
        $this->assertObjectHasAttribute('libelle', $pool);

        $this->assertEquals("xxxL", $pool->lien);
        $this->assertEquals("xxxLL", $pool->libelle);
    }

    public function testFormatUniqueSucceed()
    {
        $output = "<liste>
                        <poule>
                            <lien>xxxL</lien>
                            <libelle>xxxLL</libelle>
                        </poule>
                    </liste>";

        $arrayPools = $this->pool::format($output);

        $this->assertInternalType('array', $arrayPools);
        $this->assertTrue(is_array($arrayPools));
        $this->assertEquals(1, count($arrayPools));
        $this->assertArrayHasKey(0, $arrayPools);

        $pool = $arrayPools[0];
        $this->assertObjectHasAttribute('lien', $pool);
        $this->assertObjectHasAttribute('libelle', $pool);

        $this->assertEquals("xxxL", $pool->lien);
        $this->assertEquals("xxxLL", $pool->libelle);
    }

    /**
     * @expectedException \Exception
     */
    public function testFormatFailure()
    {
        $output = "<poule>
                        <lien>xxxL</lien>
                        <libelle>xxxLL</libelle>
                    </poule>
                    <poule>
                        <lien>xxx</lien>
                        <libelle>xxx</libelle>
                    </poule>
                    <poule>
                        <lien>xxx</lien>
                        <libelle>xxx</libelle>
                    </poule>
                    <poule>
                        <lien>xxx</lien>
                        <libelle>xxx</libelle>
                    </poule>
                    <poule>
                        <lien>xxx</lien>
                        <libelle>xxx</libelle>
                    </poule>
                    <poule>
                        <lien>xxx</lien>
                        <libelle>xxx</libelle>
                    </poule>
                    <poule>
                        <lien>xxx</lien>
                        <libelle>xxx</libelle>
                    </poule>
                    <poule>
                        <lien>xxx</lien>
                        <libelle>xxx</libelle>
                    </poule>
                    <poule>
                        <lien>xxx</lien>
                        <libelle>xxx</libelle>
                    </poule>
                    <poule>
                        <lien>xxx</lien>
                        <libelle>xxx</libelle>
                    </poule>
                    <poule>
                        <lien>xxx</lien>
                        <libelle>xxx</libelle>
                    </poule>
                    <poule>
                        <lien>xxx</lien>
                        <libelle>xxx</libelle>
                    </poule>
                    <poule>
                        <lien>xxx</lien>
                        <libelle>xxx</libelle>
                    </poule>";

        $arrayPools = $this->pool::format($output);
    }

    public function testFormatUniqueFailure()
    {
        $output = "<poule>
                        <lien>xxxL</lien>
                        <libelle>xxxLL</libelle>
                    </poule>";

        $arrayPools = $this->pool::format($output);

        $this->assertInternalType('array', $arrayPools);
        $this->assertTrue(is_array($arrayPools));
        $this->assertEquals(0, count($arrayPools));
        $this->assertArrayNotHasKey(0, $arrayPools);
    }

    public function setUp()
    {
        $this->pool = new Pool();
        parent::setUp(); // TODO: Change the autogenerated stub
    }
}