<?php

namespace App\Test\Services\Formaters;

use App\Services\Formaters\Organism;
use PHPUnit\Framework\TestCase;

class OrganismTest extends TestCase {

    private $organism;

    public function testFormatSucceed()
    {
        $output = "<liste>
                        <organisme>
                            <libelle>xxxL</libelle>
                            <id>xxxI</id>
                            <code>xxxC</code>
                            <idpere>xxxII</idpere>
                        </organisme>
                        <organisme>
                            <libelle>xxx</libelle>
                            <id>xxx</id>
                            <code>xxx</code>
                            <idpere>xxx</idpere>
                        </organisme>
                        <organisme>
                            <libelle>xxx</libelle>
                            <id>xxx</id>
                            <code>xxx</code>
                            <idpere>xxx</idpere>
                        </organisme>
                        <organisme>
                            <libelle>xxx</libelle>
                            <id>xxx</id>
                            <code>xxx</code>
                            <idpere>xxx</idpere>
                        </organisme>
                        <organisme>
                            <libelle>xxx</libelle>
                            <id>xxx</id>
                            <code>xxx</code>
                            <idpere>xxx</idpere>
                        </organisme>
                        <organisme>
                            <libelle>xxx</libelle>
                            <id>xxx</id>
                            <code>xxx</code>
                            <idpere>xxx</idpere>
                        </organisme>
                        <organisme>
                            <libelle>xxx</libelle>
                            <id>xxx</id>
                            <code>xxx</code>
                            <idpere>xxx</idpere>
                        </organisme>
                        <organisme>
                            <libelle>xxx</libelle>
                            <id>xxx</id>
                            <code>xxx</code>
                            <idpere>xxx</idpere>
                        </organisme>
                    </liste>";

        $arrayOrganisms = $this->organism::format($output);

        $this->assertInternalType('array', $arrayOrganisms);
        $this->assertTrue(is_array($arrayOrganisms));
        $this->assertEquals(8, count($arrayOrganisms));
        $this->assertArrayHasKey(0, $arrayOrganisms);

        $organism = $arrayOrganisms[0];
        $this->assertObjectHasAttribute('libelle', $organism);
        $this->assertObjectHasAttribute('id', $organism);
        $this->assertObjectHasAttribute('code', $organism);
        $this->assertObjectHasAttribute('idpere', $organism);

        $this->assertEquals("xxxL", $organism->libelle);
        $this->assertEquals("xxxI", $organism->id);
        $this->assertEquals("xxxC", $organism->code);
        $this->assertEquals("xxxII", $organism->idpere);
    }

    public function testFormatUniqueSucceed()
    {
        $output = "<liste>
                        <organisme>
                            <libelle>xxxL</libelle>
                            <id>xxxI</id>
                            <code>xxxC</code>
                            <idpere>xxxII</idpere>
                        </organisme>
                    </liste>";

        $arrayOrganisms = $this->organism::format($output);

        $this->assertInternalType('array', $arrayOrganisms);
        $this->assertTrue(is_array($arrayOrganisms));
        $this->assertEquals(1, count($arrayOrganisms));
        $this->assertArrayHasKey(0, $arrayOrganisms);

        $organism = $arrayOrganisms[0];
        $this->assertObjectHasAttribute('libelle', $organism);
        $this->assertObjectHasAttribute('id', $organism);
        $this->assertObjectHasAttribute('code', $organism);
        $this->assertObjectHasAttribute('idpere', $organism);

        $this->assertEquals("xxxL", $organism->libelle);
        $this->assertEquals("xxxI", $organism->id);
        $this->assertEquals("xxxC", $organism->code);
        $this->assertEquals("xxxII", $organism->idpere);
    }

    /**
     * @expectedException \Exception
     */
    public function testFormatFailure()
    {
        $output = "<organisme>
                        <libelle>xxxL</libelle>
                        <id>xxxI</id>
                        <code>xxxC</code>
                        <idpere>xxxII</idpere>
                    </organisme>
                    <organisme>
                        <libelle>xxx</libelle>
                        <id>xxx</id>
                        <code>xxx</code>
                        <idpere>xxx</idpere>
                    </organisme>
                    <organisme>
                        <libelle>xxx</libelle>
                        <id>xxx</id>
                        <code>xxx</code>
                        <idpere>xxx</idpere>
                    </organisme>
                    <organisme>
                        <libelle>xxx</libelle>
                        <id>xxx</id>
                        <code>xxx</code>
                        <idpere>xxx</idpere>
                    </organisme>
                    <organisme>
                        <libelle>xxx</libelle>
                        <id>xxx</id>
                        <code>xxx</code>
                        <idpere>xxx</idpere>
                    </organisme>
                    <organisme>
                        <libelle>xxx</libelle>
                        <id>xxx</id>
                        <code>xxx</code>
                        <idpere>xxx</idpere>
                    </organisme>
                    <organisme>
                        <libelle>xxx</libelle>
                        <id>xxx</id>
                        <code>xxx</code>
                        <idpere>xxx</idpere>
                    </organisme>
                    <organisme>
                        <libelle>xxx</libelle>
                        <id>xxx</id>
                        <code>xxx</code>
                        <idpere>xxx</idpere>
                    </organisme>";

        $arrayOrganisms = $this->organism::format($output);
    }

    public function testFormatUniqueFailure()
    {
        $output = "<organisme>
                        <libelle>xxxL</libelle>
                        <id>xxxI</id>
                        <code>xxxC</code>
                        <idpere>xxxII</idpere>
                    </organisme>";

        $arrayOrganisms = $this->organism::format($output);

        $this->assertInternalType('array', $arrayOrganisms);
        $this->assertTrue(is_array($arrayOrganisms));
        $this->assertEquals(0, count($arrayOrganisms));
        $this->assertArrayNotHasKey(0, $arrayOrganisms);
    }

    public function setUp()
    {
        $this->organism = new Organism();
        parent::setUp(); // TODO: Change the autogenerated stub
    }
}