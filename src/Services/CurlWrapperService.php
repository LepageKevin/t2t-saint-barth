<?php
namespace App\Services;

use Symfony\Component\HttpFoundation\JsonResponse;
use App\Services\ICurlable;

class CurlWrapperService implements ICurlable {

    /**
     * Return string of result with url
     * @param string $url
     * @param array $params
     * @return string
     */
    public function get(string $url, array $params) : string
    {
        $cURL = curl_init();

        $params = '?' . http_build_query($params);
        $this->setCurlOptions($cURL, $url, $params);
        // DEBUG
        
        /*curl_setopt($cURL, CURLOPT_VERBOSE, true);
        $verbose = fopen('php://temp', 'w+');
        curl_setopt($cURL, CURLOPT_STDERR, $verbose);
        //var_dump($cURL);
        */
        $output = curl_exec($cURL);
        // DEBUG
        /*
        if ($output === FALSE) {
            printf("cUrl error (#%d): %s<br>\n", curl_errno($cURL),
                   htmlspecialchars(curl_error($cURL)));
        }

        rewind($verbose);
        $verboseLog = stream_get_contents($verbose);

        echo "Verbose information:\n<pre>", htmlspecialchars($verboseLog), "</pre>\n";
        */
        curl_close($cURL);

        return $output;
    }

    /**
     * Set options to use curl
     * @param $cURL
     * @param string $url
     * @param string $params
     * @return CurlWrapperService
     */
    private function setCurlOptions($cURL, string $url, string $params) : CurlWrapperService
    {
        // set url
        curl_setopt($cURL, CURLOPT_URL, $url.$params);

        //return the transfer as a string
        curl_setopt($cURL, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($cURL, CURLOPT_TIMEOUT, 10); //timeout in seconds

        return $this;
    }
}