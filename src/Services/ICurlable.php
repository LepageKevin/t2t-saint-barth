<?php
namespace App\Services;

/**
 * l'interface pour faciliter les différenciations et pouvoir changer quand on veut le curlwrapper
 * Interface ICurlable
 * @package App\Services
 */
interface ICurlable {
    public function get(string $url, array $params);
}