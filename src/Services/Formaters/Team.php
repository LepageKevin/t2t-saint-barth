<?php

namespace App\Services\Formaters;

class Team implements IFormater {

    public static function format(string $output) : array
    {
        $arrayTeams = [];
        try {
            $xml = simplexml_load_string($output);
            $arrayTeams = $xml->xpath('//liste/equipe');
        } catch(\Exception $e) {
            throw new \Exception("Return not a valid xml");
        }
        return $arrayTeams;
    }
}