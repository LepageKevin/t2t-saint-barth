<?php

namespace App\Services\Formaters;

class Member implements IFormater {

    public static function format(string $output) : array
    {
        $arrayMembers = [];

        try {
            $xml = simplexml_load_string($output);
            $arrayMembers = $xml->xpath('//liste/licence');
        } catch(\Exception $e) {
            throw new \Exception("Return not a valid xml");
        }
        return $arrayMembers;
    }
}