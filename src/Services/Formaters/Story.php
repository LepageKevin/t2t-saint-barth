<?php

namespace App\Services\Formaters;

class Story implements IFormater {

    public static function format(string $output) : array
    {
        $arrayStories = [];
        try {
            $xml = simplexml_load_string($output);
            $arrayStories = $xml->xpath('//liste/news');
        } catch(\Exception $e) {
            throw new \Exception("Return not a valid xml");
        }
        return $arrayStories;
    }
}