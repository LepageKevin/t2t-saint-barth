<?php

namespace App\Services\Formaters;

class Round implements IFormater {

    public static function format(string $output) : array
    {
        $arrayRounds = [];
        try {
            $xml = simplexml_load_string($output);
            $arrayRounds = $xml->xpath('//liste/tour');
        } catch(\Exception $e) {
            throw new \Exception("Return not a valid xml");
        }
        return $arrayRounds;
    }
}