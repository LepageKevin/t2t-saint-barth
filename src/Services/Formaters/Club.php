<?php

namespace App\Services\Formaters;

class Club implements IFormater {

    public static function format(string $output) : array
    {
        $arrayClubs = [];
        try {
            $xml = simplexml_load_string($output);
            $arrayClubs = $xml->xpath('//liste/club');
        } catch(\Exception $e) {
            throw new \Exception("Return not a valid xml");
        }
        return $arrayClubs;
    }
}