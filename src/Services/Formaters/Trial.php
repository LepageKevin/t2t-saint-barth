<?php

namespace App\Services\Formaters;

class Trial implements IFormater {

    public static function format(string $output) : array
    {
        $arrayTrials = [];
        try {
            $xml = simplexml_load_string($output);
            $arrayTrials = $xml->xpath('//liste/epreuve');
        } catch(\Exception $e) {
            throw new \Exception("Return not a valid xml");
        }
        return $arrayTrials;
    }
}