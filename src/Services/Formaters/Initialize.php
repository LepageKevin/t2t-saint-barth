<?php

namespace App\Services\Formaters;

class Initialize implements IFormater {

    public static function format(string $output) : array
    {
        $arrayInitialize = [];
        try {
            $xml = simplexml_load_string($output);
            $arrayInitialize = $xml->xpath('initialisation/appli');
        } catch(\Exception $e) {
            throw new \Exception("Return not a valid xml");
        }
        return $arrayInitialize;
    }
}