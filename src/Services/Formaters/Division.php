<?php

namespace App\Services\Formaters;

class Division implements IFormater {

    public static function format(string $output) : array
    {
        $arrayDivisions = [];
        try {
            $xml = simplexml_load_string($output);
            $arrayDivisions = $xml->xpath('//liste/division');
        } catch(\Exception $e) {
            throw new \Exception("Return not a valid xml");
        }
        return $arrayDivisions;
    }
}