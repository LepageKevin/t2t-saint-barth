<?php

namespace App\Services\Formaters;

class FormaterXmlResponse implements IFormater {

    public static function format(string $output/*, string $tag*/) : array
    {
        $array = [];
        try {
            $xml = simplexml_load_string($output);
            $array = $xml->xpath('//liste/'/*$tag*/);
        } catch(\Exception $e) {
            throw new \Exception("Return not a valid xml");
        }
        return $array;
    }
}