<?php

namespace App\Services\Formaters;

class Rank implements IFormater {

    public static function format(string $output) : array
    {
        $arrayRanks = [];
        try {
            $xml = simplexml_load_string($output);
            $arrayRanks = $xml->xpath('//liste/classement');
        } catch(\Exception $e) {
            throw new \Exception("Return not a valid xml");
        }
        return $arrayRanks;
    }
}