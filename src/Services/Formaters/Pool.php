<?php

namespace App\Services\Formaters;

class Pool implements IFormater {

    public static function format(string $output) : array
    {
        $arrayPools = [];
        try {
            $xml = simplexml_load_string($output);
            $arrayPools = $xml->xpath('//liste/poule');
        } catch(\Exception $e) {
            throw new \Exception("Return not a valid xml");
        }
        return $arrayPools;
    }
}