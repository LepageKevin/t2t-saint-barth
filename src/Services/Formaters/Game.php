<?php

namespace App\Services\Formaters;

class Game implements IFormater {

    public static function format(string $output) : array
    {
        $arrayGames = [];
        try {
            $xml = simplexml_load_string($output);
            $arrayGames = $xml->xpath('//liste/partie');
        } catch(\Exception $e) {
            throw new \Exception("Return not a valid xml");
        }
        return $arrayGames;
    }
}