<?php

namespace App\Services\Formaters;

class Result implements IFormater {

    public static function format(string $output) : array
    {
        $arrayResults = [];

        try {
            $xml = simplexml_load_string($output);
            $arrayResults = $xml->xpath('//liste/resultat');
        } catch(\Exception $e) {
            throw new \Exception("Return not a valid xml");
        }
        return $arrayResults;
    }
}