<?php

namespace App\Services\Formaters;

class Historic implements IFormater {

    public static function format(string $output) : array
    {
        $arrayHistorics = [];

        try {
            $xml = simplexml_load_string($output);
            $arrayHistorics = $xml->xpath('//liste/histo');
        } catch(\Exception $e) {
            throw new \Exception("Return not a valid xml");
        }
        return $arrayHistorics;
    }
}