<?php

namespace App\Services\Formaters;

class Organism implements IFormater {

    public static function format(string $output) : array
    {
        $arrayOrganisms = [];
        try {
            $xml = simplexml_load_string($output);
            $arrayOrganisms = $xml->xpath('//liste/organisme');
        } catch(\Exception $e) {
            throw new \Exception("Return not a valid xml");
        }
        return $arrayOrganisms;
    }
}