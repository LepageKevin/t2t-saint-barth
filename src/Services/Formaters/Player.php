<?php

namespace App\Services\Formaters;

class Player implements IFormater {

    public static function format(string $output) : array
    {
        $arrayPlayers = [];

        try {
            $xml = simplexml_load_string($output);
            $arrayPlayers = $xml->xpath('//liste/joueur');
        } catch(\Exception $e) {
            throw new \Exception("Return not a valid xml");
        }
        return $arrayPlayers;
    }
}