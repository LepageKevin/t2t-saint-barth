<?php
namespace App\Services\Formaters;

interface IFormater {
    public static function format(string $output);
    //public static function formatForParent(string $output, string $tag);
}