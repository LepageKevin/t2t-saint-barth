<?php
namespace App\Services;

use App\Services\Formaters\{Historic, Organism, Initialize, Club, Trial, Division, Rank, Pool, Round, Team, Game, Player, Member, Result, Story};

class FFTTWebService {

    /**
     * Service to use cURL
     * @CurlWrapperService curlWrapperService
     */
    private $curlWrapperService;

    private $api;

    private $app_fftt_api_id;

    private $app_fftt_api_password;

    //private const SERVER_API = 'http://www.fftt.com/mobile/pxml/xml_';

    private const SERVER_API = 'https://apiv2.fftt.com/mobile/pxml/xml_';

    private const SERVER_API_EXT = '.php';

    /**
     * @param ICurlable $curlWrapper
     * @param string $api
     * @param string $app_fftt_api_id
     * @param string $app_fftt_api_password
     */
    public function setCurlWrapper(ICurlable $curlWrapper, string $api, string $app_fftt_api_id = "", string $app_fftt_api_password = "") : void
    {
        $this->curlWrapperService = $curlWrapper;
        $this->api = $api;
        $this->app_fftt_api_id = $app_fftt_api_id;
        $this->app_fftt_api_password = $app_fftt_api_password;
    }

    /**
     * Return list of clubs for one department
     *
     * @link http://www.fftt.com/mobile/pxml/xml_club_dep2.php
     *
     * @param $department
     * @return mixed
     * @throws \Exception
     */
    public function getClubsByDepartment(int $department) : array
    {
        $response = $this->callApi(
            'club_dep2',
            [
                "dep" => $department
            ]
        );
        // We have only in this call, typeclub add in the xml by club

        $arrayClubs = Club::format($response);

        return $arrayClubs;
    }

    /**
     * Return list of clubs search by numero, name, city or postal code
     *
     * @link http://www.fftt.com/mobile/pxml/xml_club_b.php
     *
     * @param int $department
     * @param string $name of club or city of club
     * @param int $number
     * @param string $postalCode
     * @return array
     * @throws \Exception
     */
    public function getClubs(int $department = 0, string $name = "", int $number = 0, string $postalCode = "") : array
    {
        $params = [];

        if (isset($department) && $department !== 0) {
            $params += [
                "dep" => $department
            ];
        }
        if (isset($name) && $name !== "") {
            $params += [
                "ville" => $name
            ];
        }
        if (isset($number) && $number !== 0) {
            $params += [
                "numero" => $number
            ];
        }
        if (isset($postalCode) && $postalCode !== "") {
            $params += [
                "code" => $postalCode
            ];
        }
        $response = $this->callApi(
            'club_b', $params
        );

        $arrayClubs = Club::format($response);

        return $arrayClubs;
    }

    /**
     * Return detail of one club
     *
     * @link http://www.fftt.com/mobile/pxml/xml_club_detail.php
     *
     * @param int $numClub
     * @return array
     * @throws \Exception
     */
    public function getDetailsByClub(int $numClub)
    {
        $response = $this->callApi(
            'club_detail',
            [
                "club" => $numClub
            ]
        );

        $arrayClub = Club::format($response);
        // Comment rendre ça mieux ? plutôt que de faire un return du premier index
        // TODO
        return $arrayClub[0];
    }

    /**
     * Return list of organims
     *
     * @link http://www.fftt.com/mobile/pxml/xml_organisme.php
     *
     * @param string $type
     * @param int $parent
     * @return array
     * @throws \Exception
     */
    public function getOrganisms(string $type, int $parent = 0) : array
    {
        $params = [
            "type" => $type
        ];

        if (isset($parent) && $parent !== 0) {
            $params += ["pere" => $parent];
        }

        $response = $this->callApi(
            'organisme',
            $params
        );

        $arrayOrganisms = Organism::format($response);

        return $arrayOrganisms;
    }

    /**
     * Return list of trials by organism
     *
     * @link http://www.fftt.com/mobile/pxml/xml_epreuve.php
     *
     * @param int $idOrganism
     * @param string $type E=Team I=Individual
     * @return array
     * @throws \Exception
     */
    public function getTrials(int $idOrganism, string $type) : array
    {
        $response = $this->callApi(
            'epreuve',
            [
                "organisme" => $idOrganism,
                "type"      => $type
            ]
        );

        $arrayTrials = Trial::format($response);

        return $arrayTrials;
    }

    /**
     * Return list of divisions by trial
     *
     * @link http://www.fftt.com/mobile/pxml/xml_division.php
     *
     * @param int $idOrganism
     * @param int $idTrial
     * @param string $type
     * @return array
     * @throws \Exception
     */
    public function getDivisions(int $idOrganism, int $idTrial, string $type) : array
    {
        $response = $this->callApi(
            'division',
            [
                "organisme" => $idOrganism,
                "epreuve"   => $idTrial,
                "type"      => $type
            ]
        );

        $arrayDivisions = Division::format($response);

        return $arrayDivisions;
    }

    /**
     * Return results by pool
     *
     * @link http://www.fftt.com/mobile/pxml/xml_result_equ.php
     *
     * @param string $action
     * @param int $idDivision
     * @param int $poolID
     * @return mixed
     * @throws \Exception
     */
    public function getPool(string $action, int $idDivision, int $poolID = -1)
    {
        $params = [
            "action"=> $action,
            "auto" => 1,
            "D1" => $idDivision
        ];

        if (isset($poolID) && $poolID !== -1) {
            $params += [
                "cx_poule" => $poolID
            ];
        }

        $response = $this->callApi(
            'result_equ',
            $params
        );

        switch ($action) {
            case "poule":
                $arrays = Pool::format($response);
                break;
            case "classement":
                $arrays = Rank::format($response);
                break;
            case "initial":
                $arrays = Rank::format($response);
                break;
            case "":
                $arrays = Round::format($response);
                break;
            default:
                throw new \Exception("Invalid option action");
        }

        return $arrays;
    }

    /**
     * Return details of match
     *
     *@link http://www.fftt.com/mobile/pxml/xml_chp_renc.php
     * @param string $parameters
     * @return array
     * @throws \Exception
     */
    public function getDetailMatch(string $parameters)
    {
        $is_retour = $phase = $res_1 = $res_2 = $renc_id = $equip_1 = $equip_2 = $equip_id1 = $equip_id2 = "";
        parse_str($parameters);

        $response = $this->callApi(
            'chp_renc',
            [
                "is_retour" => $is_retour,
                "phase"   => $phase,
                "res_1"   => $res_1,
                "res_2"   => $res_2,
                "renc_id"   => $renc_id,
                "equip_1"   => $equip_1,
                "equip_2"   => $equip_2,
                "equip_id1"   => $equip_id1,
                "equip_id2"   => $equip_id2
            ]
        );

        $arrayResults = Result::format($response);
        $arrayPlayers = Player::format($response);
        $arrayGames = Game::format($response);

        return [
            "results" => $arrayResults,
            "players" => $arrayPlayers,
            "games"   => $arrayGames
        ];

    }

    /**
     * Return list of teams by club
     *
     * @link http://www.fftt.com/mobile/pxml/xml_equipe.php
     *
     * @param int $numClub
     * @param string $typeChampionship
     * @return array
     * @throws \Exception
     */
    public function getTeamsByClub(int $numClub, string $typeChampionship = "") : array
	{
        $response = $this->callApi(
            'equipe',
            [
                "numclu" => $numClub,
                "type"   => $typeChampionship
            ]
        );

        $arrayTeams = Team::format($response);

        return $arrayTeams;
	}

    /**
     * Return result of division for individual trial
     *
     * @link http://www.fftt.com/mobile/pxml/xml_result_indiv.php
     *
     * @param string $action
     * @param int $idTrial
     * @param int $idDivision
     * @param int $idGroup
     * @return array
     * @throws \Exception
     */
    public function getResultIndividualTrial(string $action, int $idTrial, int $idDivision, int $idGroup = 0)
    {
        $params = [
            "action" => $action,
            "erp"   => $idTrial,
            "res_division"   => $idDivision
        ];

        if (isset($idGroup) && $idGroup !== 0) {
            $params += [
                "cx_tableau" => $idGroup
            ];
        }

        $response = $this->callApi(
            'result_indiv',
            $params
        );

        switch ($action) {
            case "poule":
                $arrays = Round::format($response);
                break;
            case "classement":
                $arrays = Rank::format($response);
                break;
            case "partie":
                $arrays = Game::format($response);
                break;
            default:
                throw new \Exception("Invalid option action");
        }

        return $arrays;
    }

    /**
     * Return ranks of division in criterium
     *
     * @link http://www.fftt.com/mobile/pxml/xml_res_cla.php
     *
     * @param int $idDivision
     * @return array
     * @throws \Exception
     */
    public function getRanksCriterium(int $idDivision)
    {
        // Need to verify with idDivision with new method of criterium
        $response = $this->callApi(
            'res_cla',
            [
                "res_division" => $idDivision
            ]
        );
        $arrayRanks = Rank::format($response);

        return $arrayRanks;
    }

    /**
     * Return list of players base of rank
     *
     * @link http://www.fftt.com/mobile/pxml/xml_liste_joueur.php
     *
     * @param int $numClub
     * @param string $lastName
     * @param string $firstName
     * @return array
     * @throws \Exception
     */
    public function getListPlayersByRank(int $numClub = 0, string $lastName = "", $firstName = "") : array
    {
        // TODO sa a été viré ?
        if ($numClub !== 0 || $lastName !== "") {
            $params = [];

            if ($numClub !== 0) {
                $params += [
                    "club" => $numClub
                ];
            }

            if ($lastName !== "") {
                $params += [
                    "nom" => $lastName
                ];
            }

            if ($firstName !== "") {
                $params += [
                    "prenom" => $firstName
                ];
            }

            $response = $this->callApi(
                'liste_joueur',
                $params
            );

            $arrayPlayers = Player::format($response);

            return $arrayPlayers;
        }
        throw new \Exception("Invalid call to function, need option");
    }

    /**
     * Return list of members base of SPID
     *
     * @link http://www.fftt.com/mobile/pxml/xml_liste_joueur_o.php
     *
     * @param int $numClub
     * @param int $numMember
     * @param string $lastNamePlayer
     * @param string $firstNamePlayer
     * @param int $isValid
     * @return array
     * @throws \Exception
     */
    public function getListMembers(int $numClub = 0, int $numMember = 0, string $lastNamePlayer = "", string $firstNamePlayer = "", int $isValid = -1) : array
    {
        if ($numClub !== 0 || $numClub !== 0 || $lastNamePlayer !== "") {
            $params = [];

            if ($numClub !== 0) {
                $params += [
                    "club" => $numClub
                ];
            }

            if ($numMember !== 0) {
                $params += [
                    "licence" => $numMember
                ];
            }

            if ($lastNamePlayer !== "") {
                $params += [
                    "nom" => $lastNamePlayer
                ];
            }

            if ($firstNamePlayer !== "") {
                $params += [
                    "prenom" => $firstNamePlayer
                ];
            }

            if ($isValid !== -1) {
                $params += [
                    "valid" => $isValid
                ];
            }

            $response = $this->callApi(
                'liste_joueur_o',
                $params
            );

            $arrayPlayers = Player::format($response);

            return $arrayPlayers;
        }
        throw new \Exception("Invalid call to function, need option");
    }

    /**
     * Return details of player base rank
     *
     * @link http://www.fftt.com/mobile/pxml/xml_joueur.php
     *
     * @param int $numMember
     * @return array
     * @throws \Exception
     */
    public function getPlayer(int $numMember)
    {
        $response = $this->callApi(
            'joueur',
            [
                "licence" => $numMember
            ]
        );

        $arrayPlayer = Player::format($response);

        // TODO ça retourne une 404
        return $arrayPlayer[0];
        //return $arrayPlayer;
    }

    /**
     * Return detail of member
     *
     * @link http://www.fftt.com/mobile/pxml/xml_licence.php
     *
     * @param int $numMember
     * @return array
     * @throws \Exception
     */
    public function getMember(int $numMember)
    {
        $response = $this->callApi(
            'licence',
            [
                "licence" => $numMember
            ]
        );

        $arrayMember = Member::format($response);

        // TODO
        return $arrayMember[0];
        //return $arrayMember;
    }

    /**
     * Return detail member + information about rank
     *
     * @link http://www.fftt.com/mobile/pxml/xml_licence_b.php
     *
     * @param int $numMember
     * @param int $numClub
     * @return array
     * @throws \Exception
     */
    public function getMembers(int $numMember = 0, int $numClub = 0)
    { // TODO KO with numClub
        if ($numMember !== 0) {
            $params = [
                "licence" => $numMember
            ];
        }

        if ($numClub !== 0) {
            $params = [
                "club" => $numClub
            ];
        }

        $response = $this->callApi(
            'licence_b',
            $params
        );

        $arrayMembers = Member::format($response);

        return $arrayMembers;
    }

    /**
     * Return list of matchs by player about rank
     *
     * @link http://www.fftt.com/mobile/pxml/xml_partie_mysql.php
     *
     * @param int $numMember
     * @return array
     * @throws \Exception
     */
    public function getMatchsByPlayer(int $numMember) : array
    {
        $response = $this->callApi(
            'partie_mysql',
            [
                "licence" => $numMember
            ]
        );

        $arrayMatchs = Game::format($response);

        return $arrayMatchs;
    }

    /**
     * Return list of matchs by player about SPID
     *
     * @link http://www.fftt.com/mobile/pxml/xml_partie.php
     *
     * @param int $numMember
     * @return array
     * @throws \Exception
     */
    public function getMatchsByPlayerSPID(int $numMember) : array
    {
        $response = $this->callApi(
            'partie',
            [
                "numlic" => $numMember
            ]
        );

        $arrayResults = Result::format($response);

        return $arrayResults;
    }

    /**
     * @link http://www.fftt.com/mobile/pxml/xml_new_actu.php
     * @return array
     * @throws \Exception
     */
    public function getStories() : array
    {
        $response = $this->callApi('new_actu', []);

        $arrayStories = Story::format($response);

        return $arrayStories;
    }

    /**
     * Return history rank by player
     *
     * @link http://www.fftt.com/mobile/pxml/xml_histo_classement.php
     *
     * @param int $numMember
     * @return array
     * @throws \Exception
     */
    public function getRankByPlayer(int $numMember) : array
    {
        $response = $this->callApi(
            'histo_classement',
            [
                "numlic" => $numMember
            ]
        );

        $arrayResults = Historic::format($response);

        return $arrayResults;
    }

    /**
     * @param string $url
     * @param array $params
     * @return string
     * @throws \Exception
     */
	private function callApi(string $url, array $params) : string
	{
		$this->isInitializeAPI();
		$params = array_merge($params, $this->getOptionsAPI());
        $timestamp = microtime(true);

		$a = $this->curlWrapperService->get(self::SERVER_API.$url.self::SERVER_API_EXT, $params);
        $timestamp2 = microtime(true);
        $b = $timestamp2 - $timestamp;
        //echo $b."\n";
        return $a;
	}

    /**
     * @link http://www.fftt.com/mobile/pxml/xml_initialisation.php
     *
     * @return bool
     * @throws \Exception
     */
	private function isInitializeAPI() : bool
	{
	    // TODO Create generate serial qui doit être appelé qu'une seule fois si il existe, singleton ?
		$response = $this->curlWrapperService->get(self::SERVER_API.'initialisation'.self::SERVER_API_EXT, array_merge($this->getOptionsAPI(), ["serie" => "123456781425765"]));

        // Retourne true par défaut mais on ne sait pas si l'API est OK
        return true;
        // Ancienne version de l'API
        $response = Initialize::format($response);

        if(!empty($response)) {
            if (!empty($response[0]) && !empty($response[0][0])) {
                return true;
                //TODO Traitement du retour parce qu'il y a encore un objet dans le retour
                //var_dump($response[0],$response[0]);die;
                //return $response[0][0];
            }
        }
	}

    /**
     * Get options like id for API
     * @return array
     */
    private function getOptionsAPI() : array
    {
        $timestamp = date('YmdHis') . substr(microtime(), 2, 3);
        $timestampCrpyted =  hash_hmac('sha1', $timestamp, hash('md5', $this->app_fftt_api_password, false));

        // TODO Key
        $serial = "123456781425765";

        $options = [
            "serie" => $serial,
            "tm"	=> $timestamp,
            "tmc"	=> $timestampCrpyted,
            "id"	=> $this->app_fftt_api_id
        ];
        return $options;
    }
}