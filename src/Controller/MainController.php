<?php
namespace App\Controller;

use App\Services\FFTTWebService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class MainController extends AbstractController
{
    // my num licence 4929991
    // yannick licence 4929059
    /** @var FFTTWebService  */
    private $ffttWebService;

    /**
     * MainController constructor.
     * @param FFTTWebService $ffttWebService
     */
    public function __construct(FFTTWebService $ffttWebService) {
        $this->ffttWebService = $ffttWebService;
    }

    /**
     * @Route("/list/members/{idClub}", name="list_members", requirements={"idClub"="\d+"})
     * @param int $idClub
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function findAllMembers(int $idClub)
    {
        $listMembers = $this->getAllMembersByIdClub($idClub);
        $listMembersDetails = [];
        /*foreach ($listMembers as $key => $member) {
            $listMembersDetails[] = $this->ffttWebService->getMember(intval($member->licence));
            //$listMembersDetails[] = $this->get("App\Services\FFTTWebService")->getMember(intval($member->licence));
        }*/

        return $this->render('player/members.html.twig', [
            'members' => $listMembers,
        ]);
    }

    /**
     * @Route("/player", name="search_player")
     */
    public function findPlayer() {
        return $this->render('search/searchPlayer.html.twig');
    }

    /**
     * @Route("/player/{idPlayer}", name="player", requirements={"idPlayer"="\d+"})
     */
    public function findPlayerById($idPlayer)
    {
        //$player = $this->ffttWebService->getPlayer($idPlayer);
        $player = $this->ffttWebService->getPlayer($idPlayer);
        $member = $this->ffttWebService->getMembers($idPlayer);
        return $this->render('player/player.html.twig', [
            'player' => $player,
            'member' => $member[0],
        ]);
    }

    /**
     * @Route("/club", name="search_club", methods={"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function findClub(Request $request) {

        if ($request->getMethod() =="GET") {
            $form = $this->createFormBuilder()
                ->add('name', TextType::class, array('label' => "Nom"))
                ->add('department', TextType::class, array('label' => "Département"))
                ->add('save', SubmitType::class, array('label' => 'Create Task'))
                ->getForm();

            return $this->render('search/searchClub.html.twig', array(
                'form' => $form->createView(),
            ));
        }
        else {
            $name = $request->request->get('name');
            $department = $request->request->get('department');
            if ($name !== null && $name !== '') {
                //$clubs = $this->get("App\Services\FFTTWebService")->getClubs(0, $name);
                $clubs = $this->$this->ffttWebService->getClubs(0, $name);
                return $this->render('search/clubs.html.twig', [
                    'clubs' => $clubs,
                ]);
            }
            else if ($department && $department !== '') {
                //$clubs = $this->get("App\Services\FFTTWebService")->getClubsByDepartment($department);
                $clubs = $this->ffttWebService->getClubsByDepartment($department);
                return $this->render('search/clubs.html.twig', [
                    'clubs' => $clubs,
                ]);
            }
            /*echo "<pre>";
            var_dump($request->request->get('name'));
            echo "</pre>";
            die();*/
        }

    }

    /**
     * @Route("/club/{idClub}", name="club", requirements={"idClub"="\d+"})
     */
    public function findClubById($idClub)
    {
        $oneClub = $this->ffttWebService->getDetailsByClub($idClub);
        //$oneClub = $this->get("App\Services\FFTTWebService")->getDetailsByClub($idClub);
        $count = [];

        $arrayPlayers = $this->getAllPlayersByIdClub($idClub);
        if (!empty($arrayPlayers)) {
            $count['players'] = count($arrayPlayers);
        }

        $arrayMembers = $this->getAllMembersByIdClub($idClub);
        if (!empty($arrayMembers)) {
            $count['members'] = count($arrayMembers);
        }


        return $this->render('club/club.html.twig', [
            'club' => $oneClub,
            'count' => $count,
            'players' => $arrayPlayers
        ]);
    }

    /**
     * @Route("/list/club/{numDepartment}", name="list_clubs_by_department", requirements={"numDepartment"="\d+"})
     * @param $numDepartment
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function findClubsByDepartment($numDepartment)
    {
        $clubs = $this->ffttWebService->getClubsByDepartment($numDepartment);
        //$clubs = $this->get("App\Services\FFTTWebService")->getClubsByDepartment($numDepartment);
        return $this->render('club/clubs.html.twig', [
            'clubs' => $clubs,
        ]);
    }

    /**
     * @Route("/championship/senior", name="championship_senior")
     */
    public function findSeniorChampionship()
    {
        $typeChampionship = "A";
        $typeOrganism = "E";
        $return = $this->getChampionship("FED_Championnat de France par Equipes Masculin", $typeChampionship, $typeOrganism, "");
        return $this->render('team/ranks.html.twig', $return);
    }


    /**
     * @Route("/championship/junior", name="championship_junior")
     */
    public function findJuniorChampionship()
    {
        //$idsChampionship = [3949];
        // 3949 is the id of Champ. Jeunes Départemental
        $typeChampionship = "";
        $typeOrganism = "E";
        $idEpreuve = [2685];
        // Id retourner lors du dump des teams
        $return = $this->getChampionship("FED_Championnat par Equipes Jeunes", $typeChampionship, $typeOrganism, $idEpreuve);
        return $this->render('team/ranks.html.twig', $return);
    }

    /**
     * @Route("/championship/anjou_cup", name="championship_anjou_cup")
     */
    public function findAnjouCup()
    {
        // With this API can't verify the good number about championship Junior because is there list can't find COUPES DE L'ANJOU
        $idOrganism = $this->getIdOrganismByNumDepartmentAndType("49", "D");
        if ($idOrganism == "Undefined id in list of organisms") {
            // TODO
            return "Error Page";
        }

        $trials = $this->getTrialsByIdOrganismAndType($idOrganism, "E");
        $idEpreuve = [];

        foreach ($trials as $index => $trial) {
            if (strpos($trial->libelle, "Coupe Nationale Veterans") !== false) {
                $idEpreuve[] = strval($trial->idepreuve);
            }
        }
        $idsChampionship = [3932];

        // et afficher avec lien
        // TODO enlever le dur
        //$teams = $this->get("App\Services\FFTTWebService")->getTeamsByClub(12490062, "");
        $teams = $this->ffttWebService->getTeamsByClub(12490062, "");

        $ranks = [];
        $labels = [];
        $rounds = [];
        $index = 0;

        foreach ($teams as $key => $team) {
            if (!in_array(strval($team->idepr), $idEpreuve)) {
                unset($teams[$key]);
            } else {
                $D1 = "";
                $cx_poule = "";
                parse_str(strval($team->liendivision));

                //$rank = $this->get("App\Services\FFTTWebService")->getPool("classement", $D1, $cx_poule);
                $rank = $this->ffttWebService->getPool("classement", $D1, $cx_poule);
                $ranks[] = $rank;
                //$round = $this->get("App\Services\FFTTWebService")->getPool("", $D1, $cx_poule);
                $round = $this->ffttWebService->getPool("", $D1, $cx_poule);

                // traitement des tours
                $numRound = 1;

                foreach ($round as $enj=>$oneMatch) {
                    if (strpos($oneMatch->libelle, "tour n° ".$numRound) === false) {
                        $numRound++;
                    }
                    $rounds[$index][] = $oneMatch;
                }

                $index++;
                $labels[$key]["labelDivision"] = strval($team->libdivision);
                $labels[$key]["isDown"] = false;
            }
        }
        $labels = array_merge($labels);

        $prevNb = 0;
        foreach ($rounds as $poolRound) {
            foreach ($poolRound as $round) {
                if ($prevNb != unpack("C*", $round->libelle)) {
                    $round->show = true;
                    $prevNb = unpack("C*", $round->libelle);
                }
                else {
                    $round->show = false;
                }
            }
        }

        return $this->render('team/ranks.html.twig', [
            'ranks' => $ranks,
            'labels' => $labels,
            'rounds'  => $rounds
        ]);
    }

    /**
     * @Route("/championship/criterium", name="championship_criterium")
     */
    public function findCriteriumChampionship()
    {
        $typeChampionship = "";
        $typeOrganism = "I";
        // With this API can't verify the good number about championship Junior because is there list can't find COUPES DE L'ANJOU
        $idOrganism = $this->getIdOrganismByNumDepartmentAndType("49", "D");
        if ($idOrganism == "Undefined id in list of organisms") {
            // TODO
            return "Error Page";
        }

        //$idEpreuve = [2685];
        // Id retourner lors du dump des teams
        $return = $this->getChampionship("Critérium fédéral", $typeChampionship, $typeOrganism, "");
        return $this->render('team/ranks.html.twig', $return);
    }

    /**
     * @Route("/championship/veteran", name="championship_veteran")
     */
    public function findVeteranChampionship()
    {
        $typeChampionship = "";
        $typeOrganism = "E";
        $idEpreuve = [2812];
        //$return = $this->getChampionship("FED_Coupe Nationale Vétérans", $typeChampionship, $typeOrganism, "");
        $return = $this->getChampionship("D49_Champ. vétérans", $typeChampionship, $typeOrganism, $idEpreuve);
        
        return $this->render('team/ranks.html.twig', $return);

        /*
         * 3061
         * RÉSULTAT CHAMPIONNAT DE FRANCE VÉTÉRANS A ST JULIEN TT 44 :

        - Catégorie V5 : jean siché éliminé en 1/4 finale en simple

        - Catégorie V5 double hommes : jean siché éliminé en 1/4 finale également.

        beau parcours de notre papy qui ira peut être au championnat du monde à LAS VEGAS... histoire à suivre
         */
    }

    /**
     * Return detail of one match
     * @Route("/match/{params}", name="match")
     * @param string $params
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getDetailsMatch(string $params) {
        $details = $this->ffttWebService->getDetailMatch($params);
        $players = [];
        foreach ($details['players'] as $infos) {
            $infoPlayerA = explode(" ", $infos->xca);
            $infoPlayerB = explode(" ", $infos->xcb);
            $players['A'][] = [
                'joueur' => $infos->xja,
                'sexe'   => $infoPlayerA[0],
                'points' => $infoPlayerA[1]
            ];

            $players['B'][] = [
                'joueur' => $infos->xjb,
                'sexe'   => $infoPlayerB[0],
                'points' => $infoPlayerB[1]
            ];

        }

        return $this->render('team/games.html.twig', [
            'result' => $details['results'],
            'players' => $players,
            'games' => $details['games']
        ]);
    }


    /**
     * @Route("/lucky/test", name="test")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function test(Request $request)
    {
        $detailsPool = $request->request->get('details');
        $D1 = "";
        $cx_poule = "";
        parse_str($detailsPool);

        $arrays = $this->ffttWebService->getPool("initial", intval($D1), intval($cx_poule));

        return $this->render('team/pools.html.twig', [
            'pools' => $arrays,
        ]);
    }

    /**
     * @Route("/", name="home")
     */
    public function home()
    {
        // Scan dir
        // Into boucle tywig
        $dir = "images/swiper";
        $files = scandir($dir);
        // Suppression des "." & ".." du scan
        unset($files[0]);
        unset($files[1]);
        return $this->render('home.html.twig', [
            'images' => $files,
        ]);
    }

    /**
     * @Route("/history", name="history")
     */
    public function history()
    {
        return $this->render('history.html.twig');
    }

    /**
     * @Route("/stories", name="stories")
     */
    public function stories()
    {
        //$stories = $this->get("App\Services\FFTTWebService")->getStories();
        $stories = $this->ffttWebService->getStories();
        return $this->render('story/stories.html.twig', [
            'stories' => $stories,
        ]);
    }

	/**
     * @Route("/lucky/number")
    */
    public function number()
    {
        // Faire l'appel aux services ici.

        //$clubs =  $this->get("App\Services\FFTTWebService")->getClubsByDepartment(49);

        /*$clubs = $this->get("App\Services\FFTTWebService")->getClubs(49, "ANGERS ST LEONARD", 12490029);

        return $this->render('club/clubs.html.twig', [
            'clubs' => $clubs,
        ]);*/

        /*$oneClub = $this->get("App\Services\FFTTWebService")->getDetailsByClub(12490029);
        // Return unknow when num is undefined

        return $this->render('club/club.html.twig', [
            'club' => $oneClub,
        ]);*/

        /*$organisms = $this->get("App\Services\FFTTWebService")->getOrganisms("L");

        return $this->render('organism/organisms.html.twig', [
            'organisms' => $organisms,
        ]);*/

        /*$trials = $this->get("App\Services\FFTTWebService")->getTrials(1012, "I");
        return $this->render('trial/trials.html.twig', [
            'trials' => $trials,
        ]);*/

        /*$divisions = $this->get("App\Services\FFTTWebService")->getDivisions(1012, 1073, "E");
        return $this->render('division/divisions.html.twig', [
            'divisions' => $divisions,
        ]);*/

        //TODO link
       /* $action = "";
        //$arrays = $this->get("App\Services\FFTTWebService")->getPool($action, 3013, 3123);
        $arrays = $this->get("App\Services\FFTTWebService")->getPool($action, 16519, 1322354);

        switch ($action) {
            case "poule":
                return $this->render('team/pools.html.twig', [
                    'pools' => $arrays,
                ]);
            case "classement":
                return $this->render('team/ranks.html.twig', [
                    'ranks' => $arrays,
                ]);
            case "initial":
                return $this->render('team/ranks.html.twig', [
                    'ranks' => $arrays,
                ]);
            case "":
                return $this->render('team/rounds.html.twig', [
                    'rounds' => $arrays,
                ]);
        }*/

        //$informations = $this->get("App\Services\FFTTWebService")->getDetailMatch("");

        // TODO LINK
        /*$teams = $this->get("App\Services\FFTTWebService")->getTeamsByClub(12490029, "");
        return $this->render('team/teams.html.twig', [
            'teams' => $teams,
        ]);*/

        /*$action = "partie";
        $arrays = $this->get("App\Services\FFTTWebService")->getResultIndividualTrial($action, 1072, 5473);
        switch ($action) {
            case "poule":
                return $this->render('single/rounds.html.twig', [
                    'rounds' => $arrays,
                ]);
            case "classement":
                return $this->render('single/ranks.html.twig', [
                    'ranks' => $arrays,
                ]);
            case "partie":
                return $this->render('single/games.html.twig', [
                    'games' => $arrays,
                ]);
        }*/

        /*$generalRank = $this->get("App\Services\FFTTWebService")->getRanksCriterium(3610);
        var_dump($generalRank);die;*/

        //$arrayPlayers = $this->get("App\Services\FFTTWebService")->getListPlayersByRank(0, "Lepage", "Christian");
        /*$arrayPlayers = $this->get("App\Services\FFTTWebService")->getListPlayersByRank(0, "Aureal");
        return $this->render('player/players.html.twig', [
            'players' => $arrayPlayers,
        ]);*/


        // TODO don't works with club + name or only lastname
        //$arrayPlayers = $this->get("App\Services\FFTTWebService")->getListMembers(12490029, 4938492);
        /*$arrayPlayers = $this->get("App\Services\FFTTWebService")->getListMembers(0, 0, "Lepage", "Christian");
        return $this->render('player/players.html.twig', [
            'players' => $arrayPlayers,
        ]);*/

        /*$player = $this->get("App\Services\FFTTWebService")->getPlayer(4929992);
        return $this->render('player/player.html.twig', [
            'player' => $player,
        ]);*/

        /*$stories = $this->get("App\Services\FFTTWebService")->getStories();
        return $this->render('story/stories.html.twig', [
            'stories' => $stories,
        ]);*/

        $member = $this->ffttWebService->getMember(4929992);
        return $this->render('player/member.html.twig', [
            'member' => $member,
        ]);

        /*$arrayMembers = $this->get("App\Services\FFTTWebService")->getMembers(0, 4490062);
        return $this->render('player/members.html.twig', [
            'members' => $arrayMembers,
        ]);

        /*$arrayMatchs = $this->get("App\Services\FFTTWebService")->getMatchsByPlayer(4913931);
        return $this->render('match/matchs.html.twig', [
            'matchs' => $arrayMatchs,
        ]);*/

        /*$arrayResults = $this->get("App\Services\FFTTWebService")->getMatchsByPlayerSPID(4913931);
        return $this->render('result/results.html.twig', [
            'results' => $arrayResults,
        ]);*/

        /*$arrayHistorics = $this->get("App\Services\FFTTWebService")->getRankByPlayer(4913931);
        return $this->render('historic/historics.html.twig', [
            'historics' => $arrayHistorics,
        ]);*/
        

    }

    /**
     * Call service to return allMembers by clubId
     * @param int $idClub
     * @return array
     */
    private function getAllMembersByIdClub(int $idClub) : array
    {
        //return $this->get("App\Services\FFTTWebService")->getListMembers($idClub);
        return $this->ffttWebService->getListMembers($idClub);
    }

    /**
     * Call service to return allPlayers by clubId
     * @param int $idClub
     * @return array
     */
    private function getAllPlayersByIdClub(int $idClub) : array
    {
        //return $this->get("App\Services\FFTTWebService")->getListPlayersByRank($idClub);
        return $this->ffttWebService->getListPlayersByRank($idClub);
    }

    /**
     * Return idOrganism for numero of department and type
     * @param string $numDepartment
     * @param string $type
     * @return string
     */
    private function getIdOrganismByNumDepartmentAndType(string $numDepartment = "49", string $type) : string
    {
        $organisms = $this->getOrganismByType($type);
        foreach ($organisms as $index => $organism) {
            //TODO pas en brut
            if (strval($organism->id) == $numDepartment) {
                return $organism->idPere;
            }
        }

        //TODO Exception
        return "Undefined id in list of organisms";
    }

    /**
     * Return list of organisms by type
     * @param string $type
     * @return array
     */
    private function getOrganismByType(string $type) : array
    {
        //return $this->get("App\Services\FFTTWebService")->getOrganisms($type);
        return $this->ffttWebService->getOrganisms($type);
        // Résultat pour "D"
        /*{
            +"libelle": "MAINE ET LOIRE"
            +"id": "56"
            +"code": "D49"
            +"idPere": "20"
        }*/
    }

    /**
     * Return differences trials by department and type of trials
     * @param int $idOrganism
     * @param string $type E=Team I=Individual
     * @return array
     */
    private function getTrialsByIdOrganismAndType(int $idOrganism, string $type) : array
    {
        return $this->ffttWebService->getTrials($idOrganism, $type);
    }

    /**
     * Return differences trials by department and type of trials
     * @param string $labelTrial Nom de l'épreuve recherché
     * @param string $typeChampionship Type de championnat cf la documentation FFTT
     * @param string $typeOrganism Type d'organisme cf la documentation FFTT
     * @param array $idEpreuve Identifiant de l'épreuve si non trouvé auparavant
     * @return array
     */
    private function getChampionship(string $labelTrial, string $typeChampionship = "", $typeOrganism, $idEpreuve)
    {
        $idOrganism = $this->getIdOrganismByNumDepartmentAndType("49", "D");
        if ($idOrganism == "Undefined id in list of organisms") {
            // TODO
            return "Error Page";
        }

        $trials = $this->getTrialsByIdOrganismAndType($idOrganism, $typeOrganism);

        // Identifiant de l'épreuve correspondant
        $idsChampionship = [];

        foreach ($trials as $index => $trial) {
            if (strpos($trial->libelle, $labelTrial) !== false) {
                $idsChampionship[] = strval($trial->idepreuve);
            }
        }

        if (!empty($idEpreuve)) {
            $idsChampionship = $idEpreuve;
        }

        // Si Type d'organisme est individuel on applique pour le criterium federal
        if ($typeOrganism === "I") {
            // Récupérer les joueurs qui pourraient participer aux critérium
        } else {
            // et afficher avec lien
            // TODO enlever le dur
            $teams = $this->ffttWebService->getTeamsByClub(12490062, $typeChampionship);
            $ranks = [];
            $labels = [];
            $rounds = [];
            $index = 0;

            foreach ($teams as $key => $team) {
                if (!in_array(strval($team->idepr), $idsChampionship)) {
                    unset($teams[$key]);
                } else {
                    $D1 = "";
                    $cx_poule = "";
                    parse_str(strval($team->liendivision));

                    $rank = $this->ffttWebService->getPool("classement", $D1, $cx_poule);
                    $ranks[] = $rank;
                    $round = $this->ffttWebService->getPool("", $D1, $cx_poule);

                    // traitement des tours
                    $numRound = 1;

                    foreach ($round as $enj => $oneMatch) {
                        if (strpos($oneMatch->libelle, "tour n° ".$numRound) === false) {
                            $numRound++;
                        }
                        $rounds[$index][] = $oneMatch;
                    }

                    $index++;
                    $labels[$key]["labelDivision"] = strval($team->libdivision);
                    $labels[$key]["isDown"] = true;
                }
            }
            $labels = array_merge($labels);

            $prevNb = 0;
            foreach ($rounds as $poolRound) {
                foreach ($poolRound as $round) {
                    if ($prevNb != unpack("C*", $round->libelle)) {
                        $round->show = true;
                        $prevNb = unpack("C*", $round->libelle);
                    }
                    else {
                        $round->show = false;
                    }
                }
            }
        }

        return [
            'ranks' => $ranks,
            'labels' => $labels,
            'rounds'  => $rounds
        ];
    }
}
